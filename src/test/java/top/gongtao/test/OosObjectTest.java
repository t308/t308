package top.gongtao.test;

import top.gongtao.object.OosObject;
import org.junit.Test;

/**
 * Created by gongtao on 2017/7/2.
 */
public class OosObjectTest {

    //测试获取对象
    @Test
    public void testGetObject(){
        OosObject.getObjectDemo("gongtao-top");
    }

    //测试创建文件夹
    @Test
    public void testCreateFolder(){
        OosObject.createFolderDemo("css/");
    }

    //测试删除文件
    @Test
    public void testDeleteFile(){
        OosObject.deleteFileDemo("gongtao-top");
    }

    @Test
    public void testGetAllFileList(){


        //String bucketName = "myfirstbucke";
        String bucketName = "gtbucketvideo";
        OosObject.getAllFileList(bucketName,"jike学院/");
    }

    @Test
    public void testSaveFile(){
        OosObject.upload("test1995","");
    }

}
