package top.gongtao.test;


import org.junit.Test;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by gongtao on 2017/8/18 19:00.
 */

public class SpiderTest {



        @Test
        public void strMatch() {
            String phone = "13539770000";
            //检查phone是否是合格的手机号(标准:1开头，第二位为3,5,8，后9位为任意数字)
            System.out.println(phone + ":" + phone.matches("1[358][0-9]{9,9}")); //true

            String str = "abcd12345efghijklmn";
            //检查str中间是否包含12345
            System.out.println(str + ":" + str.matches("\\w+12345\\w+")); //true
            System.out.println(str + ":" + str.matches("\\w+123456\\w+")); //false
        }
        @Test
        public void strSplit() {
            String str = "asfasf.sdfsaf.sdfsdfas.asdfasfdasfd.wrqwrwqer.asfsafasf.safgfdgdsg";
            String[] strs = str.split("\\.");
            for (String s : strs){
                System.out.println(s);
            }
        }

        @Test
        public void getStrings() {
            String str = "rrwerqq84461376qqasfdasdfrrwerqq84461377qqasfdasdaa654645aafrrwerqq84461378qqasfdaa654646aaasdfrrwerqq84461379qqasfdasdfrrwerqq84461376qqasfdasdf";
            Pattern p = Pattern.compile("qq(.*?)qq");
            Matcher m = p.matcher(str);
            ArrayList<String> strs = new ArrayList<String>();
            while (m.find()) {
                strs.add(m.group(1));
            }
            for (String s : strs){
                System.out.println(s);
            }
        }

        @Test
        public void replace() {
            String str = "asfas5fsaf5s4fs6af.sdaf.asf.wqre.qwr.fdsf.asf.asf.asf";
            //将字符串中的.替换成_，因为.是特殊字符，所以要用\.表达，又因为\是特殊字符，所以要用\\.来表达.
            str = str.replaceAll("\\.", "_");
            System.out.println(str);
        }

        @Test
        public void testrepis(){

        }

        @Test
        public void captureHtml() throws Exception {
            //String strURL = "http://ip.chinaz.com/47.93.31.184";
            //String strURL = "http://exam.bdqn.cn/testing/index/32802/e144d082f51b474492cbd59504e4f629";
            // String strURL = "http://exam.bdqn.cn:80/testing/skillList/jineng/24";
            String strURL = "http://exam.bdqn.cn/testing/skillList/jineng/24";
            URL url = new URL(strURL);

            HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
            httpConn.setRequestProperty("Cookie", "JSESSIONID=1CD7AB74BEBA3D29EC46BE49E56E393C.exam-tomcat-node2.exam-tomcat-node2; examLoginUserName=\"330556994@qq.com\"");

            //        httpConn.set
            InputStreamReader input = new InputStreamReader(httpConn
                    .getInputStream(), "utf-8");
            BufferedReader bufReader = new BufferedReader(input);
            String line = "";
            StringBuilder contentBuf = new StringBuilder();

//        String regex_email= "\\w+@\\w+(\\.[a-zA-Z]{2,3}){1,2}";
            String regex_email= "([a-b]{2,3}){1,2}";
            String reg = "\\<img\\s+src=.*?\\s*\\/?\\>\\i";
            List<String> list= new ArrayList<>();
            BufferedWriter out = out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("text.txt", false)));
            Pattern pattern= Pattern.compile(regex_email);
            while ((line = bufReader.readLine()) != null) {
//            Matcher matcher = pattern.matcher(line);
//                while(matcher.find()){
//                    contentBuf.append(matcher.group());
//                }

                if(line.contains("【课程】")){
                    contentBuf.append(line.substring(0,line.length()-11).trim()+"\n");
                }
                //if(line.contains("30px\">")){
                    //contentBuf.append(line);
                //}
                try {



                    out.write(line);
                    out.newLine();
                    out.flush();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
//                try {
//                    if(out != null){
//                        //out.close();
//                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
                }
            }
            String buf = contentBuf.toString();



//        int beginIx = buf.indexOf("查询结果[");
//        int endIx = buf.indexOf("上面四项依次显示的是");
//        String result = buf.substring(beginIx, endIx);
//        System.out.println("captureHtml()的结果：\n" + result);
            System.out.println(buf);
        }
    }
