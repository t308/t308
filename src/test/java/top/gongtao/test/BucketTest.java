package top.gongtao.test;

import top.gongtao.object.BucketManager;
import org.junit.Test;

/**
 * Created by gongtao on 2017/7/2.
 * 阿里云  Bucket 测试
 */
public class BucketTest {

    //测试获取Bucket列表
    @Test
    public void testGetBucektList(){
        BucketManager.getBucketList();
    }

    //测试创建 Bucket
    @Test
    public void testAddBucket() {
        BucketManager.createBucket("gongtao-top-static2","public-read");
    }

    //测试移除 Bucket
    @Test
    public void testRemoveBucket(){
        BucketManager.removeBucket("gongtao-top-static2");
    }

    //测试指定 Bucket 是否存在
    @Test
    public void testIsExistBucket(){
        BucketManager.IsExistBucket("myfirstbucke");
    }

    //设置指定 Bucket ACI权限
    @Test
    public void testChangeBucketACI(){
        BucketManager.changeBucketACI("gongtao-top","public-read-write");
    }

    //设置指定 Bucket ACI权限
    @Test
    public void testGetBucketACI(){
        BucketManager.getBucketACI("gongtao-top");
    }

    //获取指定 Bucket Location
    @Test
    public void testGetBucketLocation(){
        BucketManager.getBucketLocation("gongtao-top");
    }

    //获取指定 Bucket info
    @Test
    public void testGetBucketInfo(){
        BucketManager.getBucketInfo("gongtao-top");
    }





}
