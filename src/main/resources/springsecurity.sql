
/*用户表*/
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `ID` INT(10) NOT NULL AUTO_INCREMENT COMMENT '用户编号，主键',
  `USERNAME` VARCHAR(50) NOT NULL COMMENT '用户名',
  `PASSWORD` VARCHAR(50) NOT NULL COMMENT '密码',
  `EMAIL` VARCHAR(50) NOT NULL COMMENT '邮箱',
  `MOBILE` VARCHAR(11) DEFAULT NULL COMMENT '手机号',
  `SEX` TINYINT(1) DEFAULT '0' COMMENT '性别  1男  2女',
  `AGE` TINYINT(3) DEFAULT '0' COMMENT '年龄',
  `NICKNAME` VARCHAR(50) DEFAULT NULL COMMENT '显示名 （昵称）',
  `HEADIMG` VARCHAR(255) DEFAULT NULL COMMENT '用户头像',
  `MSGNUM` INT(11) DEFAULT '0' COMMENT '站内信未读消息数',
  `BIRTHDAY` DATETIME DEFAULT NULL COMMENT '生日',
  `CREATETIME` TIMESTAMP NULL DEFAULT NULL COMMENT '注册时间',
  `ISAVALIBLE` TINYINT(1) DEFAULT '1' COMMENT '是否可用 1正常  2冻结',
  `LOCKREASON` VARCHAR(200) NULL COMMENT '账号被禁用原因'
  `LASTLOGINTIME` TIMESTAMP NULL DEFAULT NULL COMMENT '最后登录时间',
  `LASTLOGINIP` VARCHAR(20) DEFAULT NULL COMMENT '最后登录IP',
  `FANSCOUNT` INT(11) DEFAULT '0' COMMENT "粉丝数量",
  `ATTENTIONCOUNT` INT(11) DEFAULT '0' COMMENT "关注数量",
  PRIMARY KEY (`ID`)
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户表';

/*角色表*/
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `ID` INT(10) NOT NULL AUTO_INCREMENT COMMENT '角色编号',
  `NAME` VARCHAR(50) NOT NULL COMMENT '角色名',
  `ENABLED` TINYINT(1) DEFAULT '1' COMMENT "是否被禁用，0禁用 1正常"
  PRIMARY KEY (`ID`)
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='角色表';

/*权限表*/
DROP TABLE IF EXISTS `sys_permission`;
CREATE TABLE `sys_permission` (
  `ID` INT(10) NOT NULL AUTO_INCREMENT COMMENT '权限编号',
  `NAME` VARCHAR(50) NOT NULL COMMENT '权限名称',
  `DESCRIPTION` VARCHAR(200) NOT NULL COMMENT '权限描述',
  `URL` VARCHAR(100) NOT NULL COMMENT '权限路径',
  `PID` INT(10) NULL COMMENT '父节点ID',
  `ENABLED` TINYINT(1) DEFAULT '1' COMMENT "是否被禁用，0禁用 1正常"
  PRIMARY KEY (`ID`)
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='权限表';

/*角色用户表*/
DROP TABLE IF EXISTS `sys_role_user`;
CREATE TABLE `sys_role_user` (
  `ID` INT(10) NOT NULL AUTO_INCREMENT COMMENT '角色用户编号',
  `SYS_USER_ID` INT(10) NOT NULL COMMENT '用户编号',
  `SYS_ROLE_ID` INT(10) NOT NULL COMMENT '角色编号',
  PRIMARY KEY (`ID`)
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='角色用户表';

/*角色权限表*/
DROP TABLE IF EXISTS `sys_permission_role`;
CREATE TABLE `sys_permission_role` (
  `ID` INT(10) NOT NULL AUTO_INCREMENT COMMENT '角色权限编号',
  `ROLE_ID` INT(10) NOT NULL COMMENT '角色编号',
  `PERMISSION_ID` INT(10) NOT NULL COMMENT '权限编号',
  PRIMARY KEY (`ID`)
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='角色权限表';

/*友情链接表*/
DROP TABLE IF EXISTS `gt_link`;
CREATE TABLE `gt_link` (
  `ID` INT(10) NOT NULL AUTO_INCREMENT COMMENT '链接编号',
  `TEXT` VARCHAR(50) NOT NULL COMMENT '链接文字',
  `URL` VARCHAR(200) NOT NULL COMMENT '链接地址',
  `DESCRIPTION` VARCHAR(500) NULL COMMENT '链接描述',
  `IMAGEURL` VARCHAR(200) NOT NULL COMMENT '链接图片路径',
  `VIEWCOUNT` INT(10) NOT NULL DEFAULT '0' COMMENT '链接访问次数',
  PRIMARY KEY (`ID`)
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='友情链接表';

/*用户表插入数据*/
INSERT INTO SYS_USER (id,username, PASSWORD) VALUES (1,'admin', 'admin');
INSERT INTO SYS_USER (id,username, PASSWORD) VALUES (2,'gongtao', 'gongtao');

/*角色表插入数据*/
INSERT INTO SYS_ROLE(id,`NAME`) VALUES(1,'ROLE_ADMIN');
INSERT INTO SYS_ROLE(id,`NAME`) VALUES(2,'ROLE_USER');

/*角色用户表插入数据*/
INSERT INTO SYS_ROLE_USER(SYS_USER_ID,SYS_ROLE_ID) VALUES(1,1);
INSERT INTO SYS_ROLE_USER(SYS_USER_ID,SYS_ROLE_ID) VALUES(2,2);

/*权限表插入数据*/
BEGIN;
INSERT INTO `Sys_permission`(`ID`,`NAME`,`DESCRIPTION`,`URL`,`PID`) VALUES ('1', 'ROLE_HOME', 'home', '/', NULL), ('2', 'ROLE_ADMIN', 'ABel', '/admin', NULL);
COMMIT;

/*权限角色表插入数据*/
BEGIN;
INSERT INTO `Sys_permission_role`(`ID`,`ROLE_ID`,`PERMISSION_ID`) VALUES ('1', '1', '1'), ('2', '1', '2'), ('3', '2', '1');
COMMIT;

INSERT INTO `gt_link`(`text`,`url`,`imageurl`,`description`) VALUES
('阿里云','https://www.aliyun.com/','',''),
('腾讯云','https://cloud.tencent.com/','',''),
('百度云','https://cloud.baidu.com/','',''),
('新浪云','http://www.sinacloud.com/','',''),
('华为云','http://www.hwclouds.com/','',''),
('京东云','http://www.jcloud.com/index','',''),
('小鸟云','https://www.niaoyun.com/','',''),
('青云','https://www.qingcloud.com/','',''),
('网易云','https://www.163yun.com/','',''),
('AWS 云服务','https://amazonaws-china.com/cn/','',''),
('美团云','https://www.mtyun.com/','',''),
('UCLOUD','https://incubator.ucloud.cn/','',''),
('CSDN','http://www.csdn.net/','',''),
('博客园','https://www.cnblogs.com/','',''),
('菜鸟教程','http://www.runoob.com/','',''),
('东翌编程','http://www.dongyibiancheng.com/','',''),
('开源中国社区','http://www.oschina.net/','',''),
('爱词霸在线翻译','http://www.iciba.com/','',''),
('Github','https://github.com/','',''),
('CODING','https://coding.net/','',''),
('码云','https://git.oschina.net/','',''),
('百度开发者中心','https://developer.baidu.com/','',''),
('百度开发者平台','http://app.baidu.com/','',''),
('微信公众平台','https://mp.weixin.qq.com/','',''),
('微信开放平台','https://open.weixin.qq.com/','',''),
('微盟','http://www.weimob.com/website/index.html','',''),
('百度地图开放平台','http://lbsyun.baidu.com/','',''),
('Git','https://git-scm.com/','',''),
('牛客网','https://www.nowcoder.com/','',''),
('Rythm模板引擎','http://rythmengine.org/','',''),
('脚本之家','http://www.jb51.net/','',''),
('IDEA','https://www.jetbrains.com/idea/','',''),
('DaoCloud','https://www.daocloud.io/','',''),
('Oracle Cloud企业云计算','','https://cloud.oracle.com/zh_CN/home',''),
('易百教程','http://www.yiibai.com/','',''),
('51CTO','http://www.51cto.com/','',''),
('千库网','http://588ku.com/','',''),
('Staticfile CDN','https://www.staticfile.org/','',''),
('推酷','http://www.tuicool.com/','',''),
('数字未来','https://www.bbspro.net/','',''),
('HBuilder','http://dcloud.io/','',''),
('安卓网','http://www.hiapk.com/','',''),
('Android Studio','http://www.android-studio.org/','',''),
('W3school','http://www.w3school.com.cn/','',''),
('Oracle','https://www.oracle.com/index.html','',''),
('MySql','https://www.mysql.com/','',''),
('SqlServer 技术文档','https://docs.microsoft.com/zh-cn/sql/sql-server/sql-server-technical-documentation','',''),
('SqlServer 2016','https://www.microsoft.com/zh-cn/sql-server/sql-server-2016','',''),
('H2数据库引擎','http://www.h2database.com/html/main.html','',''),
('Hibernate','http://hibernate.org/orm/','',''),
('Struts','http://struts.apache.org/','',''),
('MyBatis','http://www.mybatis.org/mybatis-3/zh/index.html','',''),
('Druid','http://druid.io/','',''),
('黑客派','https://hacpai.com/','',''),
('SegmentFault','https://segmentfault.com/','',''),
('在线工具开源中国','http://tool.oschina.net/','',''),
('BootStrap中文网','http://www.bootcss.com/','',''),
('Iteye Java编程','http://www.iteye.com/','',''),
('Beetl','http://ibeetl.com/','',''),
('最代码','http://www.zuidaima.com/','',''),
('Nutz社区','https://nutz.cn/','',''),
('CocoaChina','http://www.cocoachina.com/','',''),
('W3Schools在线Web教程','http://w3schools.bootcss.com','',''),
('深度开源','http://www.open-open.com/','',''),
('ckplayer','http://www.ckplayer.com/','',''),
('Preloaders','https://preloaders.net/','',''),
('QUARTZ','http://www.quartz-scheduler.org/','',''),
('angularjs 中文api','http://www.angularjsapi.cn','',''),
('NodeJS中文社区','http://cnodejs.org/','',''),
('Vue.js','https://cn.vuejs.org/','',''),
('npm','https://www.npmjs.com/','',''),
('Flat UI','https://designmodo.com/flat/','',''),
('Java全栈工程师','http://how2j.cn/','',''),
('ASP.NET','https://www.asp.net/','',''),
('MVNrepository','https://mvnrepository.com/','',''),
('jQuery','http://jquery.com/','',''),
('jQuery API 中文文档','http://jquery.cuishifeng.cn/','',''),
('jQuery插件库','http://www.jq22.com/','',''),
('jQuery API 中文文档','http://www.jquery123.com/','',''),
('jQuery 之家','http://www.htmleaf.com/jQuery/','',''),
('学UI网','http://app.xueui.cn/','',''),
('React 中文','http://react-china.org/','',''),
('廖雪峰的官方网站','https://www.liaoxuefeng.com/','',''),
('程序员之家论坛','http://bbs.it-home.org/','',''),
('Stack Overflow','https://stackoverflow.com/','',''),
('282万个编程源码资料联合开发网','http://www.pudn.com/','',''),
('CodeProject','https://www.codeproject.com/','',''),
('LintCode','http://www.lintcode.com/zh-cn/','',''),
('IdeOne','http://ideone.com/','',''),
('tutorialspoint','http://www.tutorialspoint.com/','',''),
('前端乱炖','http://www.html-js.com/','',''),
('掘金','https://juejin.im/','',''),
('UI制造者','http://www.uimaker.com/','',''),
('V2EX','https://www.v2ex.com/','',''),
('泡在网上的日子','http://www.jcodecraeer.com/','',''),
('程序员在线工具','http://www.ofmonkey.com/','',''),
('Swagger UI','https://swagger.io/swagger-ui/','',''),
('简书','http://www.jianshu.com/','',''),
('2次方','http://www.2cifang.com/','',''),
('慕课网','http://www.imooc.com/','',''),
('','','',''),
('','','',''),
('','','',''),('','','',''),('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),
('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),
('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),
('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),
('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),
('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),
('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),
('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),
('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),
('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),
('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),
('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),
('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),
('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),
('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),
('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),
('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),
('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),
('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),
('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),
('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),
('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),
('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),
('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),
('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),('','','',''),



('Spring Boot MyBatis通用Mapper插件集成','https://yq.aliyun.com/articles/5831?spm=5176.100239.blogcont5564.9.MdSUmH','',''),
('SpringBoot集成Swagger','https://yq.aliyun.com/articles/5564?spm=5176.100239.blogcont5831.11.WCU5u7','',''),
('springBoot+springSecurity 数据库动态管理用户、角色、权限（二）','http://blog.csdn.net/u012373815/article/details/54633046','',''),
('springboot集成shiro 实现权限控制','http://blog.csdn.net/u012373815/article/details/57532292','',''),
('使用StringTokenizer分解字符串','https://yq.aliyun.com/articles/4799?spm=5176.100239.blogcont5564.8.17G9Hm','',''),
('使用ASM操作Java字节码，实现AOP原理','https://yq.aliyun.com/articles/4798?spm=5176.100239.blogcont4799.6.dgwYv9','',''),
('Swagger UI教程 API 文档神器 搭配Node使用','http://www.jianshu.com/p/d6626e6bd72c','',''),
('Swagger-UI 基于REST的API测试/文档类插件','https://my.oschina.net/ekc/blog/83281','',''),
('Swagger学习','http://www.cnblogs.com/tency/p/6245099.html','',''),
('SpringMVC + Swagger UI生成可视图的API文档（详细图解）','http://blog.csdn.net/u011499992/article/details/53455144','',''),
('spring security的csrf防御机制在ajax中的应用','http://blog.csdn.net/starrrr2/article/details/50074445','',''),
('页面获取Spring Security登录用户','http://blog.csdn.net/zmx729618/article/details/51914836','',''),
('spring-boot通过HttpSessionListener监听器统计在线人数','http://blog.csdn.net/zhangjq520/article/details/53670966','',''),
('SpringBoot--添加配置Servlet,Filter,listener','http://blog.csdn.net/cb2474600377/article/details/54628770','',''),
('浏览器关闭后，Session会话结束了么？','http://blog.csdn.net/stanxl/article/details/47105051','',''),
('MySQL命令行导出数据库','http://www.cnblogs.com/Vitus_feng/archive/2010/05/21/1741262.html','',''),
('Mysql热备份总结','http://blog.csdn.net/junjieguo/article/details/7823594','',''),
('Google Guava官方教程（中文版）','http://ifeve.com/google-guava/','',''),
('使用guava带来的方便','http://iamzhongyong.iteye.com/blog/1981199','',''),
('ECharts简单演示','http://blog.csdn.net/u011499992/article/details/55003182','',''),
('echarts','http://echarts.baidu.com/index.html','',''),
('spring boot 日志配置','http://www.cnblogs.com/zb38/p/5411701.html','',''),
('spring boot 基础之使用AOP统一处理请求日志使用方法','http://blog.csdn.net/qq_33565798/article/details/76779976','',''),
('Spring Boot日志管理','http://www.jianshu.com/p/59787a123b05','',''),
('spring boot 学习--07---配置文件处理','http://blog.csdn.net/javastudyr/article/details/52526220','',''),
('@Controller和@RestController的区别？','http://blog.csdn.net/gg12365gg/article/details/51345601','',''),
('SpringBoot 中常用注解@PathVaribale/@RequestParam/@GetMapping介绍`dbgirl`','http://blog.csdn.net/u010412719/article/details/69788227','',''),
('Spring Boot中使用Spring-data-jpa让数据访问更简单、更优雅','http://www.jianshu.com/p/38d27b633d9c','',''),
('Springboot 之 Hibernate自动建表（Mysql）','http://blog.csdn.net/zsl129/article/details/52880811','',''),
('spring boot 学习--08---搭建ssmm-01','http://blog.csdn.net/javastudyr/article/details/52585770','',''),
('','','',''),

('扣丁学堂','http://www.codingke.com/','',''),
('千峰教育','http://www.mobiletrain.org/','',''),
('并发编程网','http://ifeve.com/','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('视频video标签在移动端的播放总结','https://segmentfault.com/a/1190000007189004','',''),
('Spring Security应用开发(09)密码错误次数限制','http://www.cnblogs.com/coe2coe/p/6881920.html','',''),
('spring security实现限制登录次数功能','http://www.cnblogs.com/hongxf1990/p/6592686.html','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','http://www.52svn.com/index.php/Home/Svn/project_list','',''),
('','https://github.com/t308/t308','',''),
('','https://coding.net/u/t308/p/t308/git','',''),
('','https://git.oschina.net/t308/t308','',''),
('','http://www.svnchina.com/project.php','',''),
('','http://code.taobao.org/p/t308/src/','',''),
('','https://code.aliyun.com/dashboard/projects','',''),
('','https://code.csdn.net/dashboard/index','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','http://code.taobao.org/svn/t308/','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('','','',''),
('Maven','http://maven.apache.org/','',''),
('Gradle','https://gradle.org/','',''),
('Thymeleaf','http://www.thymeleaf.org/','',''),
('Spring','https://spring.io/','',''),
('Spring Boot','http://projects.spring.io/spring-boot/','',''),
('shiro','http://shiro.apache.org/','',''),
('Apache Shiro开源文档','http://www.open-open.com/doc/list/256','',''),
('让Apache Shiro保护你的应用','http://www.infoq.com/cn/articles/apache-shiro/','',''),
('将 Shiro 作为应用的权限基础','https://www.ibm.com/developerworks/cn/opensource/os-cn-shiro/','',''),
('跟我学《Shiro》','http://jinnianshilongnian.iteye.com/category/305053','',''),
('Apache Shiro 简介','https://www.ibm.com/developerworks/cn/web/wa-apacheshiro/','',''),
('Spring Security','http://projects.spring.io/spring-security/','',''),

SELECT * FROM sys_user;
SELECT * FROM sys_role;
SELECT * FROM sys_permission;
SELECT * FROM sys_role_user;
SELECT * FROM sys_permission_role;

SELECT * FROM USER WHERE location ='青岛';
