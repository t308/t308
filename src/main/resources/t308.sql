DROP TABLE IF EXISTS `lx_videocategory`;
CREATE TABLE `lx_videocategory` (
  `ID` INT(10) NOT NULL AUTO_INCREMENT COMMENT '分类编号',
  `TITLE` VARCHAR(200) NOT NULL DEFAULT '' COMMENT '分类名称',
  PRIMARY KEY (`ID`)
) ENGINE=INNODB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='视频分类表';

INSERT INTO lx_videocategory(`title`) VALUES('Android开发工程师')

SELECT * FROM lx_videocategory;


DROP TABLE IF EXISTS `lx_video`;
CREATE TABLE `lx_video` (
  `ID` INT(10) NOT NULL AUTO_INCREMENT COMMENT '视频编号',
  `PARENTNODEID` INT(10) DEFAULT '0' NOT NULL COMMENT '父节点ID',
  `ISAVALIABLE` INT(10) NOT NULL DEFAULT '0' COMMENT '1正常2删除',
  `ADDTIME` TIMESTAMP NULL DEFAULT NULL COMMENT '添加时间',
  `TITLE` VARCHAR(200) NOT NULL DEFAULT '' COMMENT '视频简介',
  `CONTEXT` LONGTEXT NOT NULL COMMENT '课程详情',
  `URL` LONGTEXT NOT NULL COMMENT '视频URL',
  `LOGO` VARCHAR(200) NOT NULL DEFAULT '' COMMENT '图片路径',
  `UPDATETIME` TIMESTAMP NULL DEFAULT NULL COMMENT '最后更新时间',
  `PAGEVIEWCOUNT` INT(11) NOT NULL DEFAULT '0' COMMENT '浏览数量',
  PRIMARY KEY (`ID`)
) ENGINE=INNODB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='视频表';
/*Data for the table `edu_course` */

INSERT  INTO `lx_video`(`ISAVALIABLE`,`ADDTIME`,`TITLE`,`CONTEXT`,`URL`,`LOGO`,`UPDATETIME`,`PAGEVIEWCOUNT`)
VALUES ('1',NOW(),'1.在Windows中下载与安装JDK.mp4','这是详情','http://gtbucketvideo.oss-cn-shanghai.aliyuncs.com/jike%E5%AD%A6%E9%99%A2/Android%E5%BC%80%E5%8F%91%E5%B7%A5%E7%A8%8B%E5%B8%88/%E7%AC%AC1%E9%98%B6%E6%AE%B5-%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/1.Android%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E4%B8%8ESDK/1.%E5%9C%A8Windows%E4%B8%AD%E4%B8%8B%E8%BD%BD%E4%B8%8E%E5%AE%89%E8%A3%85JDK.mp4','%E5%9C%A8windows%E4%B8%AD%E4%B8%8B%E8%BD%BD%E4%B8%8E%E5%AE%89%E8%A3%85jdk.png',NOW(),0)
,('1',NOW(),'2.在Mac OSX中下载与安装JDK.mp4','这是详情','http://gtbucketvideo.oss-cn-shanghai.aliyuncs.com/jike%E5%AD%A6%E9%99%A2/Android%E5%BC%80%E5%8F%91%E5%B7%A5%E7%A8%8B%E5%B8%88/%E7%AC%AC1%E9%98%B6%E6%AE%B5-%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/1.Android%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E4%B8%8ESDK/3.%E4%BD%BF%E7%94%A8ADT%E6%90%AD%E5%BB%BAAndroid%20L%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83.mp4','%E5%9C%A8windows%E4%B8%AD%E4%B8%8B%E8%BD%BD%E4%B8%8E%E5%AE%89%E8%A3%85jdk.png',NOW(),0)
,('1',NOW(),'3.使用ADT搭建Android L开发环境.mp4','这是详情','http://gtbucketvideo.oss-cn-shanghai.aliyuncs.com/jike%E5%AD%A6%E9%99%A2/Android%E5%BC%80%E5%8F%91%E5%B7%A5%E7%A8%8B%E5%B8%88/%E7%AC%AC1%E9%98%B6%E6%AE%B5-%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/1.Android%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E4%B8%8ESDK/3.%E4%BD%BF%E7%94%A8ADT%E6%90%AD%E5%BB%BAAndroid%20L%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83.mp4','%E5%9C%A8windows%E4%B8%AD%E4%B8%8B%E8%BD%BD%E4%B8%8E%E5%AE%89%E8%A3%85jdk.png',NOW(),0)
,('1',NOW(),'4.SDK的版本分析与安装.mp4','这是详情','http://gtbucketvideo.oss-cn-shanghai.aliyuncs.com/jike%E5%AD%A6%E9%99%A2/Android%E5%BC%80%E5%8F%91%E5%B7%A5%E7%A8%8B%E5%B8%88/%E7%AC%AC1%E9%98%B6%E6%AE%B5-%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/1.Android%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E4%B8%8ESDK/4.SDK%E7%9A%84%E7%89%88%E6%9C%AC%E5%88%86%E6%9E%90%E4%B8%8E%E5%AE%89%E8%A3%85.mp4','',NOW(),0)
,('1',NOW(),'5.创建和使用标准Android模拟器.mp4','这是详情','http://gtbucketvideo.oss-cn-shanghai.aliyuncs.com/jike%E5%AD%A6%E9%99%A2/Android%E5%BC%80%E5%8F%91%E5%B7%A5%E7%A8%8B%E5%B8%88/%E7%AC%AC1%E9%98%B6%E6%AE%B5-%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/1.Android%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E4%B8%8ESDK/5.%E5%88%9B%E5%BB%BA%E5%92%8C%E4%BD%BF%E7%94%A8%E6%A0%87%E5%87%86Android%E6%A8%A1%E6%8B%9F%E5%99%A8.mp4','',NOW(),0)
,('1',NOW(),'6.Intel Atom x86模拟器的安装与使用.mp4','这是详情','http://gtbucketvideo.oss-cn-shanghai.aliyuncs.com/jike%E5%AD%A6%E9%99%A2/Android%E5%BC%80%E5%8F%91%E5%B7%A5%E7%A8%8B%E5%B8%88/%E7%AC%AC1%E9%98%B6%E6%AE%B5-%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/1.Android%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E4%B8%8ESDK/6.Intel%20Atom%20x86%E6%A8%A1%E6%8B%9F%E5%99%A8%E7%9A%84%E5%AE%89%E8%A3%85%E4%B8%8E%E4%BD%BF%E7%94%A8.mp4','',NOW(),0)
,('1',NOW(),'7.Genymotion模拟器的安装与使用.mp4','这是详情','http://gtbucketvideo.oss-cn-shanghai.aliyuncs.com/jike%E5%AD%A6%E9%99%A2/Android%E5%BC%80%E5%8F%91%E5%B7%A5%E7%A8%8B%E5%B8%88/%E7%AC%AC1%E9%98%B6%E6%AE%B5-%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/1.Android%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E4%B8%8ESDK/7.Genymotion%E6%A8%A1%E6%8B%9F%E5%99%A8%E7%9A%84%E5%AE%89%E8%A3%85%E4%B8%8E%E4%BD%BF%E7%94%A8.mp4','',NOW(),0)
,('1',NOW(),'8.在Mac OSX中配置与使用adb命令行工具.mp4','这是详情','http://gtbucketvideo.oss-cn-shanghai.aliyuncs.com/jike%E5%AD%A6%E9%99%A2/Android%E5%BC%80%E5%8F%91%E5%B7%A5%E7%A8%8B%E5%B8%88/%E7%AC%AC1%E9%98%B6%E6%AE%B5-%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/1.Android%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E4%B8%8ESDK/8.%E5%9C%A8Mac%20OSX%E4%B8%AD%E9%85%8D%E7%BD%AE%E4%B8%8E%E4%BD%BF%E7%94%A8adb%E5%91%BD%E4%BB%A4%E8%A1%8C%E5%B7%A5%E5%85%B7.mp4','',NOW(),0)
,('1',NOW(),'9.在Windows中配置与使用adb命令行工具.mp4','这是详情','http://gtbucketvideo.oss-cn-shanghai.aliyuncs.com/jike%E5%AD%A6%E9%99%A2/Android%E5%BC%80%E5%8F%91%E5%B7%A5%E7%A8%8B%E5%B8%88/%E7%AC%AC1%E9%98%B6%E6%AE%B5-%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/1.Android%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E4%B8%8ESDK/9.%E5%9C%A8Windows%E4%B8%AD%E9%85%8D%E7%BD%AE%E4%B8%8E%E4%BD%BF%E7%94%A8adb%E5%91%BD%E4%BB%A4%E8%A1%8C%E5%B7%A5%E5%85%B7.mp4','',NOW(),0)

,('1',NOW(),'0.课程简介.mp4','这是详情','http://gtbucketvideo.oss-cn-shanghai.aliyuncs.com/jike%E5%AD%A6%E9%99%A2/Android%E5%BC%80%E5%8F%91%E5%B7%A5%E7%A8%8B%E5%B8%88/%E7%AC%AC1%E9%98%B6%E6%AE%B5-%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/1.Android%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E4%B8%8ESDK/3.%E4%BD%BF%E7%94%A8ADT%E6%90%AD%E5%BB%BAAndroid%20L%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83.mp4','',NOW(),0)
,('1',NOW(),'1.最新安卓Android L开发环境搭建下载安装配置.mp4','这是详情','http://gtbucketvideo.oss-cn-shanghai.aliyuncs.com/jike%E5%AD%A6%E9%99%A2/Android%E5%BC%80%E5%8F%91%E5%B7%A5%E7%A8%8B%E5%B8%88/%E7%AC%AC1%E9%98%B6%E6%AE%B5-%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/1.Android%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E4%B8%8ESDK/3.%E4%BD%BF%E7%94%A8ADT%E6%90%AD%E5%BB%BAAndroid%20L%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83.mp4','',NOW(),0)

,('1',NOW(),'0.课程简介.mp4','这是详情','http://gtbucketvideo.oss-cn-shanghai.aliyuncs.com/jike%E5%AD%A6%E9%99%A2/Android%E5%BC%80%E5%8F%91%E5%B7%A5%E7%A8%8B%E5%B8%88/%E7%AC%AC1%E9%98%B6%E6%AE%B5-%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/1.Android%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E4%B8%8ESDK/3.%E4%BD%BF%E7%94%A8ADT%E6%90%AD%E5%BB%BAAndroid%20L%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83.mp4','',NOW(),0)
,('1',NOW(),'1.Android Studio下载,安装,配置.mp4','这是详情','http://gtbucketvideo.oss-cn-shanghai.aliyuncs.com/jike%E5%AD%A6%E9%99%A2/Android%E5%BC%80%E5%8F%91%E5%B7%A5%E7%A8%8B%E5%B8%88/%E7%AC%AC1%E9%98%B6%E6%AE%B5-%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/1.Android%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E4%B8%8ESDK/3.%E4%BD%BF%E7%94%A8ADT%E6%90%AD%E5%BB%BAAndroid%20L%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83.mp4','',NOW(),0)

,('1',NOW(),'0.课程简介.mp4','这是详情','http://gtbucketvideo.oss-cn-shanghai.aliyuncs.com/jike%E5%AD%A6%E9%99%A2/Android%E5%BC%80%E5%8F%91%E5%B7%A5%E7%A8%8B%E5%B8%88/%E7%AC%AC1%E9%98%B6%E6%AE%B5-%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/1.Android%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E4%B8%8ESDK/3.%E4%BD%BF%E7%94%A8ADT%E6%90%AD%E5%BB%BAAndroid%20L%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83.mp4','',NOW(),0)
,('1',NOW(),'1.英特尔Intel Atom Android模拟器下载安装配置.mp4','这是详情','http://gtbucketvideo.oss-cn-shanghai.aliyuncs.com/jike%E5%AD%A6%E9%99%A2/Android%E5%BC%80%E5%8F%91%E5%B7%A5%E7%A8%8B%E5%B8%88/%E7%AC%AC1%E9%98%B6%E6%AE%B5-%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/1.Android%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E4%B8%8ESDK/3.%E4%BD%BF%E7%94%A8ADT%E6%90%AD%E5%BB%BAAndroid%20L%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83.mp4','',NOW(),0)

,('1',NOW(),'1.建立一个HelloWorld程序.mp4','这是详情','http://gtbucketvideo.oss-cn-shanghai.aliyuncs.com/jike%E5%AD%A6%E9%99%A2/Android%E5%BC%80%E5%8F%91%E5%B7%A5%E7%A8%8B%E5%B8%88/%E7%AC%AC1%E9%98%B6%E6%AE%B5-%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/1.Android%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E4%B8%8ESDK/3.%E4%BD%BF%E7%94%A8ADT%E6%90%AD%E5%BB%BAAndroid%20L%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83.mp4','',NOW(),0)
,('1',NOW(),'2.在真机中调试和运行程序.mp4','这是详情','http://gtbucketvideo.oss-cn-shanghai.aliyuncs.com/jike%E5%AD%A6%E9%99%A2/Android%E5%BC%80%E5%8F%91%E5%B7%A5%E7%A8%8B%E5%B8%88/%E7%AC%AC1%E9%98%B6%E6%AE%B5-%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/1.Android%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E4%B8%8ESDK/3.%E4%BD%BF%E7%94%A8ADT%E6%90%AD%E5%BB%BAAndroid%20L%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83.mp4','',NOW(),0)
,('1',NOW(),'3.将应用打包发布为APK文件.mp4','这是详情','http://gtbucketvideo.oss-cn-shanghai.aliyuncs.com/jike%E5%AD%A6%E9%99%A2/Android%E5%BC%80%E5%8F%91%E5%B7%A5%E7%A8%8B%E5%B8%88/%E7%AC%AC1%E9%98%B6%E6%AE%B5-%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/5.Android%E5%BA%94%E7%94%A8%E7%A8%8B%E5%BA%8F%E7%9A%84%E6%89%93%E5%8C%85%E4%B8%8E%E5%8F%91%E5%B8%83/3.%E5%B0%86%E5%BA%94%E7%94%A8%E6%89%93%E5%8C%85%E5%8F%91%E5%B8%83%E4%B8%BAAPK%E6%96%87%E4%BB%B6.mp4','',NOW(),0)

,('1',NOW(),'2.在Mac OSX中下载与安装JDK.mp4','这是详情','http://gtbucketvideo.oss-cn-shanghai.aliyuncs.com/jike%E5%AD%A6%E9%99%A2/Android%E5%BC%80%E5%8F%91%E5%B7%A5%E7%A8%8B%E5%B8%88/%E7%AC%AC1%E9%98%B6%E6%AE%B5-%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/1.Android%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E4%B8%8ESDK/3.%E4%BD%BF%E7%94%A8ADT%E6%90%AD%E5%BB%BAAndroid%20L%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83.mp4','',NOW(),0)
,('1',NOW(),'2.在Mac OSX中下载与安装JDK.mp4','这是详情','http://gtbucketvideo.oss-cn-shanghai.aliyuncs.com/jike%E5%AD%A6%E9%99%A2/Android%E5%BC%80%E5%8F%91%E5%B7%A5%E7%A8%8B%E5%B8%88/%E7%AC%AC1%E9%98%B6%E6%AE%B5-%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/1.Android%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E4%B8%8ESDK/3.%E4%BD%BF%E7%94%A8ADT%E6%90%AD%E5%BB%BAAndroid%20L%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83.mp4','',NOW(),0)
,('1',NOW(),'2.在Mac OSX中下载与安装JDK.mp4','这是详情','http://gtbucketvideo.oss-cn-shanghai.aliyuncs.com/jike%E5%AD%A6%E9%99%A2/Android%E5%BC%80%E5%8F%91%E5%B7%A5%E7%A8%8B%E5%B8%88/%E7%AC%AC1%E9%98%B6%E6%AE%B5-%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/1.Android%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E4%B8%8ESDK/3.%E4%BD%BF%E7%94%A8ADT%E6%90%AD%E5%BB%BAAndroid%20L%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83.mp4','',NOW(),0)
,('1',NOW(),'2.在Mac OSX中下载与安装JDK.mp4','这是详情','http://gtbucketvideo.oss-cn-shanghai.aliyuncs.com/jike%E5%AD%A6%E9%99%A2/Android%E5%BC%80%E5%8F%91%E5%B7%A5%E7%A8%8B%E5%B8%88/%E7%AC%AC1%E9%98%B6%E6%AE%B5-%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/1.Android%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E4%B8%8ESDK/3.%E4%BD%BF%E7%94%A8ADT%E6%90%AD%E5%BB%BAAndroid%20L%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83.mp4','',NOW(),0)
,('1',NOW(),'2.在Mac OSX中下载与安装JDK.mp4','这是详情','http://gtbucketvideo.oss-cn-shanghai.aliyuncs.com/jike%E5%AD%A6%E9%99%A2/Android%E5%BC%80%E5%8F%91%E5%B7%A5%E7%A8%8B%E5%B8%88/%E7%AC%AC1%E9%98%B6%E6%AE%B5-%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/1.Android%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E4%B8%8ESDK/3.%E4%BD%BF%E7%94%A8ADT%E6%90%AD%E5%BB%BAAndroid%20L%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83.mp4','',NOW(),0)
,('1',NOW(),'2.在Mac OSX中下载与安装JDK.mp4','这是详情','http://gtbucketvideo.oss-cn-shanghai.aliyuncs.com/jike%E5%AD%A6%E9%99%A2/Android%E5%BC%80%E5%8F%91%E5%B7%A5%E7%A8%8B%E5%B8%88/%E7%AC%AC1%E9%98%B6%E6%AE%B5-%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/1.Android%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E4%B8%8ESDK/3.%E4%BD%BF%E7%94%A8ADT%E6%90%AD%E5%BB%BAAndroid%20L%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83.mp4','',NOW(),0)
,('1',NOW(),'2.在Mac OSX中下载与安装JDK.mp4','这是详情','http://gtbucketvideo.oss-cn-shanghai.aliyuncs.com/jike%E5%AD%A6%E9%99%A2/Android%E5%BC%80%E5%8F%91%E5%B7%A5%E7%A8%8B%E5%B8%88/%E7%AC%AC1%E9%98%B6%E6%AE%B5-%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/1.Android%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E4%B8%8ESDK/3.%E4%BD%BF%E7%94%A8ADT%E6%90%AD%E5%BB%BAAndroid%20L%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83.mp4','',NOW(),0)
,('1',NOW(),'2.在Mac OSX中下载与安装JDK.mp4','这是详情','http://gtbucketvideo.oss-cn-shanghai.aliyuncs.com/jike%E5%AD%A6%E9%99%A2/Android%E5%BC%80%E5%8F%91%E5%B7%A5%E7%A8%8B%E5%B8%88/%E7%AC%AC1%E9%98%B6%E6%AE%B5-%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/1.Android%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E4%B8%8ESDK/3.%E4%BD%BF%E7%94%A8ADT%E6%90%AD%E5%BB%BAAndroid%20L%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83.mp4','',NOW(),0)
,('1',NOW(),'2.在Mac OSX中下载与安装JDK.mp4','这是详情','http://gtbucketvideo.oss-cn-shanghai.aliyuncs.com/jike%E5%AD%A6%E9%99%A2/Android%E5%BC%80%E5%8F%91%E5%B7%A5%E7%A8%8B%E5%B8%88/%E7%AC%AC1%E9%98%B6%E6%AE%B5-%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/1.Android%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E4%B8%8ESDK/3.%E4%BD%BF%E7%94%A8ADT%E6%90%AD%E5%BB%BAAndroid%20L%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83.mp4','',NOW(),0)
,('1',NOW(),'2.在Mac OSX中下载与安装JDK.mp4','这是详情','http://gtbucketvideo.oss-cn-shanghai.aliyuncs.com/jike%E5%AD%A6%E9%99%A2/Android%E5%BC%80%E5%8F%91%E5%B7%A5%E7%A8%8B%E5%B8%88/%E7%AC%AC1%E9%98%B6%E6%AE%B5-%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/1.Android%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E4%B8%8ESDK/3.%E4%BD%BF%E7%94%A8ADT%E6%90%AD%E5%BB%BAAndroid%20L%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83.mp4','',NOW(),0)
,('1',NOW(),'2.在Mac OSX中下载与安装JDK.mp4','这是详情','http://gtbucketvideo.oss-cn-shanghai.aliyuncs.com/jike%E5%AD%A6%E9%99%A2/Android%E5%BC%80%E5%8F%91%E5%B7%A5%E7%A8%8B%E5%B8%88/%E7%AC%AC1%E9%98%B6%E6%AE%B5-%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/1.Android%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E4%B8%8ESDK/3.%E4%BD%BF%E7%94%A8ADT%E6%90%AD%E5%BB%BAAndroid%20L%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83.mp4','',NOW(),0)
,('1',NOW(),'2.在Mac OSX中下载与安装JDK.mp4','这是详情','http://gtbucketvideo.oss-cn-shanghai.aliyuncs.com/jike%E5%AD%A6%E9%99%A2/Android%E5%BC%80%E5%8F%91%E5%B7%A5%E7%A8%8B%E5%B8%88/%E7%AC%AC1%E9%98%B6%E6%AE%B5-%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/1.Android%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E4%B8%8ESDK/3.%E4%BD%BF%E7%94%A8ADT%E6%90%AD%E5%BB%BAAndroid%20L%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83.mp4','',NOW(),0)
,('1',NOW(),'2.在Mac OSX中下载与安装JDK.mp4','这是详情','http://gtbucketvideo.oss-cn-shanghai.aliyuncs.com/jike%E5%AD%A6%E9%99%A2/Android%E5%BC%80%E5%8F%91%E5%B7%A5%E7%A8%8B%E5%B8%88/%E7%AC%AC1%E9%98%B6%E6%AE%B5-%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/1.Android%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E4%B8%8ESDK/3.%E4%BD%BF%E7%94%A8ADT%E6%90%AD%E5%BB%BAAndroid%20L%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83.mp4','',NOW(),0)
,('1',NOW(),'2.在Mac OSX中下载与安装JDK.mp4','这是详情','http://gtbucketvideo.oss-cn-shanghai.aliyuncs.com/jike%E5%AD%A6%E9%99%A2/Android%E5%BC%80%E5%8F%91%E5%B7%A5%E7%A8%8B%E5%B8%88/%E7%AC%AC1%E9%98%B6%E6%AE%B5-%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/1.Android%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E4%B8%8ESDK/3.%E4%BD%BF%E7%94%A8ADT%E6%90%AD%E5%BB%BAAndroid%20L%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83.mp4','',NOW(),0)
,('1',NOW(),'2.在Mac OSX中下载与安装JDK.mp4','这是详情','http://gtbucketvideo.oss-cn-shanghai.aliyuncs.com/jike%E5%AD%A6%E9%99%A2/Android%E5%BC%80%E5%8F%91%E5%B7%A5%E7%A8%8B%E5%B8%88/%E7%AC%AC1%E9%98%B6%E6%AE%B5-%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/1.Android%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E4%B8%8ESDK/3.%E4%BD%BF%E7%94%A8ADT%E6%90%AD%E5%BB%BAAndroid%20L%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83.mp4','',NOW(),0)
,('1',NOW(),'2.在Mac OSX中下载与安装JDK.mp4','这是详情','http://gtbucketvideo.oss-cn-shanghai.aliyuncs.com/jike%E5%AD%A6%E9%99%A2/Android%E5%BC%80%E5%8F%91%E5%B7%A5%E7%A8%8B%E5%B8%88/%E7%AC%AC1%E9%98%B6%E6%AE%B5-%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/1.Android%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E4%B8%8ESDK/3.%E4%BD%BF%E7%94%A8ADT%E6%90%AD%E5%BB%BAAndroid%20L%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83.mp4','',NOW(),0)
,('1',NOW(),'2.在Mac OSX中下载与安装JDK.mp4','这是详情','http://gtbucketvideo.oss-cn-shanghai.aliyuncs.com/jike%E5%AD%A6%E9%99%A2/Android%E5%BC%80%E5%8F%91%E5%B7%A5%E7%A8%8B%E5%B8%88/%E7%AC%AC1%E9%98%B6%E6%AE%B5-%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/1.Android%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E4%B8%8ESDK/3.%E4%BD%BF%E7%94%A8ADT%E6%90%AD%E5%BB%BAAndroid%20L%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83.mp4','',NOW(),0)
,('1',NOW(),'2.在Mac OSX中下载与安装JDK.mp4','这是详情','http://gtbucketvideo.oss-cn-shanghai.aliyuncs.com/jike%E5%AD%A6%E9%99%A2/Android%E5%BC%80%E5%8F%91%E5%B7%A5%E7%A8%8B%E5%B8%88/%E7%AC%AC1%E9%98%B6%E6%AE%B5-%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/1.Android%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E4%B8%8ESDK/3.%E4%BD%BF%E7%94%A8ADT%E6%90%AD%E5%BB%BAAndroid%20L%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83.mp4','',NOW(),0)
,('1',NOW(),'2.在Mac OSX中下载与安装JDK.mp4','这是详情','http://gtbucketvideo.oss-cn-shanghai.aliyuncs.com/jike%E5%AD%A6%E9%99%A2/Android%E5%BC%80%E5%8F%91%E5%B7%A5%E7%A8%8B%E5%B8%88/%E7%AC%AC1%E9%98%B6%E6%AE%B5-%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/1.Android%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E4%B8%8ESDK/3.%E4%BD%BF%E7%94%A8ADT%E6%90%AD%E5%BB%BAAndroid%20L%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83.mp4','',NOW(),0)
,('1',NOW(),'2.在Mac OSX中下载与安装JDK.mp4','这是详情','http://gtbucketvideo.oss-cn-shanghai.aliyuncs.com/jike%E5%AD%A6%E9%99%A2/Android%E5%BC%80%E5%8F%91%E5%B7%A5%E7%A8%8B%E5%B8%88/%E7%AC%AC1%E9%98%B6%E6%AE%B5-%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/1.Android%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E4%B8%8ESDK/3.%E4%BD%BF%E7%94%A8ADT%E6%90%AD%E5%BB%BAAndroid%20L%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83.mp4','',NOW(),0)
,('1',NOW(),'2.在Mac OSX中下载与安装JDK.mp4','这是详情','http://gtbucketvideo.oss-cn-shanghai.aliyuncs.com/jike%E5%AD%A6%E9%99%A2/Android%E5%BC%80%E5%8F%91%E5%B7%A5%E7%A8%8B%E5%B8%88/%E7%AC%AC1%E9%98%B6%E6%AE%B5-%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/1.Android%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E4%B8%8ESDK/3.%E4%BD%BF%E7%94%A8ADT%E6%90%AD%E5%BB%BAAndroid%20L%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83.mp4','',NOW(),0)
,('1',NOW(),'2.在Mac OSX中下载与安装JDK.mp4','这是详情','http://gtbucketvideo.oss-cn-shanghai.aliyuncs.com/jike%E5%AD%A6%E9%99%A2/Android%E5%BC%80%E5%8F%91%E5%B7%A5%E7%A8%8B%E5%B8%88/%E7%AC%AC1%E9%98%B6%E6%AE%B5-%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/1.Android%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E4%B8%8ESDK/3.%E4%BD%BF%E7%94%A8ADT%E6%90%AD%E5%BB%BAAndroid%20L%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83.mp4','',NOW(),0)
,('1',NOW(),'2.在Mac OSX中下载与安装JDK.mp4','这是详情','http://gtbucketvideo.oss-cn-shanghai.aliyuncs.com/jike%E5%AD%A6%E9%99%A2/Android%E5%BC%80%E5%8F%91%E5%B7%A5%E7%A8%8B%E5%B8%88/%E7%AC%AC1%E9%98%B6%E6%AE%B5-%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/1.Android%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E4%B8%8ESDK/3.%E4%BD%BF%E7%94%A8ADT%E6%90%AD%E5%BB%BAAndroid%20L%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83.mp4','',NOW(),0)
,('1',NOW(),'2.在Mac OSX中下载与安装JDK.mp4','这是详情','http://gtbucketvideo.oss-cn-shanghai.aliyuncs.com/jike%E5%AD%A6%E9%99%A2/Android%E5%BC%80%E5%8F%91%E5%B7%A5%E7%A8%8B%E5%B8%88/%E7%AC%AC1%E9%98%B6%E6%AE%B5-%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/1.Android%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E4%B8%8ESDK/3.%E4%BD%BF%E7%94%A8ADT%E6%90%AD%E5%BB%BAAndroid%20L%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83.mp4','',NOW(),0)
,('1',NOW(),'2.在Mac OSX中下载与安装JDK.mp4','这是详情','http://gtbucketvideo.oss-cn-shanghai.aliyuncs.com/jike%E5%AD%A6%E9%99%A2/Android%E5%BC%80%E5%8F%91%E5%B7%A5%E7%A8%8B%E5%B8%88/%E7%AC%AC1%E9%98%B6%E6%AE%B5-%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/1.Android%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E4%B8%8ESDK/3.%E4%BD%BF%E7%94%A8ADT%E6%90%AD%E5%BB%BAAndroid%20L%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83.mp4','',NOW(),0)

SELECT * FROM lx_video;



DROP TABLE IF EXISTS `gt_images`;
/*添加banner图片表*/
CREATE TABLE `gt_images` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT,
  `URL` VARCHAR(200) DEFAULT '' COMMENT '图片地址',
  `LINKADDRESS` VARCHAR(255) DEFAULT NULL COMMENT '图链接地址',
  `TITLE` VARCHAR(255) DEFAULT NULL COMMENT '图标题',
  `TYPEID` INT(11) DEFAULT '0' COMMENT '图片类型',
  `SERIESNUMBER` INT(11) DEFAULT '0' COMMENT '序列号',
  `PREVIEWURL` VARCHAR(255) DEFAULT NULL COMMENT '略缩图片地址',
  `publishtime` DATE DEFAULT NULL COMMENT '发布时间',
  `describe` VARCHAR(5000) DEFAULT '' COMMENT '图片描述',
  `TEMPLATEID` INT(11) DEFAULT NULL COMMENT '页面模板id',
  PRIMARY KEY (`ID`)
) ENGINE=INNODB AUTO_INCREMENT=2870 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='banner图管理';

INSERT  INTO `gt_images`(`ID`,`URL`,`LINKADDRESS`,`TITLE`,`TYPEID`,`SERIESNUMBER`,`PREVIEWURL`,`publishtime`,`describe`,`TEMPLATEID`)
VALUES (1,'//myfirstbucke.oss-cn-shanghai.aliyuncs.com/Image/1.jpg',NULL,'后会无期',0,1,NULL,NULL,'亲爱的酷头们',1);

SELECT * FROM gt_images;

DROP TABLE IF EXISTS `gt_images_default`;
/*添加默认banner图片表 用于还原*/
CREATE TABLE `gt_images_default` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT,
  `URL` VARCHAR(200) DEFAULT '' COMMENT '图片地址',
  `LINKADDRESS` VARCHAR(255) DEFAULT NULL COMMENT '图链接地址',
  `TITLE` VARCHAR(255) DEFAULT NULL COMMENT '图标题',
  `TYPEID` INT(11) DEFAULT '0' COMMENT '图片类型',
  `SERIESNUMBER` INT(11) DEFAULT '0' COMMENT '序列号',
  `PREVIEWURL` VARCHAR(255) DEFAULT NULL COMMENT '略缩图片地址',
  `publishtime` DATE DEFAULT NULL COMMENT '发布时间',
  `describe` VARCHAR(5000) DEFAULT '' COMMENT '图片描述',
  `TEMPLATEURL` VARCHAR(300) DEFAULT NULL COMMENT '页面模板路径（还原用，根据路径加载数据，重新添加记录）',
  PRIMARY KEY (`ID`)
) ENGINE=INNODB AUTO_INCREMENT=2870 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='banner图管理';

INSERT  INTO `gt_images_default`(`ID`,`URL`,`LINKADDRESS`,`TITLE`,`TYPEID`,`SERIESNUMBER`,`PREVIEWURL`,`publishtime`,`describe`,`TEMPLATEURL`)
VALUES (1,'//myfirstbucke.oss-cn-shanghai.aliyuncs.com/Image/1.jpg',NULL,'后会无期',0,1,NULL,NULL,'亲爱的酷头们','/template/templet1/关于我们_找到我们.html');

SELECT * FROM gt_images_default;

DROP TABLE IF EXISTS `gt_images_type`;
/*图片类型表*/
CREATE TABLE `gt_images_type` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT COMMENT '类型ID',
  `NAME` VARCHAR(50) DEFAULT NULL COMMENT '类型名',
  PRIMARY KEY (`ID`)
) ENGINE=INNODB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COMMENT='图片类型表';

INSERT  INTO `gt_images_type`(`ID`,`NAME`) VALUES (1,'首页模板一Banner图片');

SELECT * FROM gt_images_type;


DROP TABLE IF EXISTS `gt_article`;
/*文章信息表*/
CREATE TABLE `gt_article` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT COMMENT '文章ID',
  `TITLE` VARCHAR(100) DEFAULT NULL COMMENT '文章标题',
  `SUMMARY` VARCHAR(200) DEFAULT NULL COMMENT '文章摘要',
  `KEYWORD` VARCHAR(50) DEFAULT NULL COMMENT '文章关键字',
  `IMAGEURL` VARCHAR(100) DEFAULT NULL COMMENT '文章图片URL',
  `SOURCE` VARCHAR(50) DEFAULT NULL COMMENT '文章来源',
  `AUTHOR` VARCHAR(10) DEFAULT NULL COMMENT '文章作者',
  `CREATETIME` TIMESTAMP NULL DEFAULT NULL COMMENT '文章创建时间',
  `PUBLISHTIME` TIMESTAMP NULL DEFAULT NULL COMMENT '文章发布时间',
  `PUBLISHSTATE` TINYINT(1) DEFAULT '0' COMMENT '文章发布状态 1未发布 2已发布',
  `LINK` VARCHAR(100) DEFAULT NULL COMMENT '文章访问链接',
  `ARTICLETYPE` TINYINT(4) DEFAULT '0' COMMENT '文章类型 2文章',
  `CLICKNUM` INT(11) DEFAULT '0' COMMENT '文章点击量',
  `PRAISECOUNT` INT(11) DEFAULT '0' COMMENT '点赞数量',
  `COMMENTNUM` INT(11) DEFAULT '0' COMMENT '评论数',
  `SORT` INT(11) DEFAULT '0' COMMENT '排序值',
  `COLUMNID` INT(11) DEFAULT NULL COMMENT '栏目ID',
  PRIMARY KEY (`ID`)
) ENGINE=INNODB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COMMENT='文章信息表';

INSERT  INTO `gt_article`(`ID`,`TITLE`,`SUMMARY`,`KEYWORD`,`IMAGEURL`,`SOURCE`,`AUTHOR`,`CREATETIME`,`PUBLISHTIME`,`PUBLISHSTATE`,`LINK`,`ARTICLETYPE`,`CLICKNUM`,`PRAISECOUNT`,`COMMENTNUM`,`SORT`,`COLUMNID`) VALUES (7,'中国首款学习习近平重要讲话APP上线','“学习中国”APP得到了国家互联网信息办公室的支持与指导。','学习中国,学习习近平','/images/upload/article/20161124/1479957884001.jpg','inxedu','lmxxxaa','2015-03-24 01:13:37','2015-09-09 10:08:03',2,'/15/03/25/1427247760573.html',2,91,1,0,0,NULL);

SELECT * FROM `gt_article`;

DROP TABLE IF EXISTS `gt_article_content`;
/*文章内容表*/
CREATE TABLE `gt_article_content` (
  `ID` INT(11) DEFAULT '0' COMMENT '文章ID',
  `CONTENT` TEXT COMMENT '文章内容'
) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='文章内容表';
INSERT  INTO `gt_article_content`(`ID`,`CONTENT`) VALUES (7,'这是文章内容');

SELECT * FROM `gt_article_content`;


DROP TABLE IF EXISTS `gt_sys_log`;
/*后台系统操作日志*/
CREATE TABLE `gt_sys_log` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `adminUserId` INT(11) DEFAULT NULL COMMENT '后台用户id',
  `create_time` TIMESTAMP NULL DEFAULT NULL COMMENT '创建时间',
  `content` TEXT COMMENT '描述内容',
  `operation` VARCHAR(100) DEFAULT NULL COMMENT '操作类型',
  `type` VARCHAR(50) DEFAULT NULL COMMENT '类型：add添加 update 更新 del 删除操作',
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='';

DROP TABLE IF EXISTS `sys_login_log`;
/*后台登陆日志*/
CREATE TABLE `sys_login_log` (
  `LOG_ID` INT(11) NOT NULL AUTO_INCREMENT,
  `LOGIN_TIME` TIMESTAMP NULL DEFAULT NULL COMMENT '登录时间',
  `IP` VARCHAR(20) DEFAULT NULL COMMENT '登录IP',
  `USER_ID` INT(11) DEFAULT '0' COMMENT '用户ID',
  `OS_NAME` VARCHAR(50) DEFAULT NULL COMMENT '操作系统',
  `USER_AGENT` VARCHAR(50) DEFAULT NULL COMMENT '浏览器',
  PRIMARY KEY (`LOG_ID`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;






