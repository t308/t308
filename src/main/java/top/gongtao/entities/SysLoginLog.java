package top.gongtao.entities;

import java.util.Date;

/**
 * Created by gongtao on 2017/8/13 16:25.
 * 后台登陆日志实体类
 */

public class SysLoginLog {
    //日志ID
    private Integer id;
    //登陆时间
    private Date logintime;
    //用户IP
    private String ip;
    //用户ID
    private Integer userid;
    //操作系统
    private String osname;
    //浏览器
    private String useragent;



}
