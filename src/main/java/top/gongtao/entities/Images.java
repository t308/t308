package top.gongtao.entities;

import java.util.Date;

/**
 * Created by gongtao on 2017/8/1.
 * 图片实体类
 */
public class Images {

    private Integer id;
    private String url;
    private String linkaddress;
    private String title;
    private Integer typeid;
    private Integer seriesnumber;
    private String previewurl;
    private Date publishdate;
    private String describe;
    private Integer templateid;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLinkaddress() {
        return linkaddress;
    }

    public void setLinkaddress(String linkaddress) {
        this.linkaddress = linkaddress;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getTypeid() {
        return typeid;
    }

    public void setTypeid(Integer typeid) {
        this.typeid = typeid;
    }

    public Integer getSeriesnumber() {
        return seriesnumber;
    }

    public void setSeriesnumber(Integer seriesnumber) {
        this.seriesnumber = seriesnumber;
    }

    public String getPreviewurl() {
        return previewurl;
    }

    public void setPreviewurl(String previewurl) {
        this.previewurl = previewurl;
    }

    public Date getPublishdate() {
        return publishdate;
    }

    public void setPublishdate(Date publishdate) {
        this.publishdate = publishdate;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public Integer getTemplateid() {
        return templateid;
    }

    public void setTemplateid(Integer templateid) {
        this.templateid = templateid;
    }
}
