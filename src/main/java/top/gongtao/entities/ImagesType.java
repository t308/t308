package top.gongtao.entities;

/**
 * Created by gongtao on 2017/8/13 16:13.
 * 图片类型实体
 */

public class ImagesType {

    //  类型 ID
    private Integer id;

    //  类型名称
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
