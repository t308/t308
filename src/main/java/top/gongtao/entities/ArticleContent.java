package top.gongtao.entities;

/**
 * Created by gongtao on 2017/8/13 16:23.
 * 文章内容实体类
 */

public class ArticleContent {

    //文章ID
    private Integer id;
    //文章类容
    private String content;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}

