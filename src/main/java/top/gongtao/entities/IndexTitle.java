package top.gongtao.entities;

/**
 * Created by gongtao on 2017/8/2.
 * 首页标题实体类
 */
public class IndexTitle {

    private Integer id;
    private String title;
    private Integer parentnodeid;
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    private Integer status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getParentnodeid() {
        return parentnodeid;
    }

    public void setParentnodeid(Integer parentnodeid) {
        this.parentnodeid = parentnodeid;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
