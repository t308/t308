package top.gongtao.entities;

import java.util.Date;

/**
 * Created by gongtao on 2017/8/13 16:14.
 * 文章信息实体类
 */

public class Article {

    private Integer id;             //文章ID
    private String title;           //文章标题
    private String summary;         //文章摘要
    private String keyword;         //文章关键字
    private String imageurl;        //文章图片URL
    private String source;          //文章来源
    private String author;          //文章作者
    private Date createtime;        //文章创建时间
    private Date publishtime;       //文章发布时间
    private Integer publishstate;   //文章发布状态
    private String link;            //文章访问链接
    private Integer articletype;    //文章类型
    private Integer clicknum;       //文章点击量
    private Integer praisecount;    //点赞数量
    private Integer commentnum;     //评论书
    private String sort;            //排序值
    private String columnid;        //栏目ID

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public Date getPublishtime() {
        return publishtime;
    }

    public void setPublishtime(Date publishtime) {
        this.publishtime = publishtime;
    }

    public Integer getPublishstate() {
        return publishstate;
    }

    public void setPublishstate(Integer publishstate) {
        this.publishstate = publishstate;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Integer getArticletype() {
        return articletype;
    }

    public void setArticletype(Integer articletype) {
        this.articletype = articletype;
    }

    public Integer getClicknum() {
        return clicknum;
    }

    public void setClicknum(Integer clicknum) {
        this.clicknum = clicknum;
    }

    public Integer getPraisecount() {
        return praisecount;
    }

    public void setPraisecount(Integer praisecount) {
        this.praisecount = praisecount;
    }

    public Integer getCommentnum() {
        return commentnum;
    }

    public void setCommentnum(Integer commentnum) {
        this.commentnum = commentnum;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getColumnid() {
        return columnid;
    }

    public void setColumnid(String columnid) {
        this.columnid = columnid;
    }
}
