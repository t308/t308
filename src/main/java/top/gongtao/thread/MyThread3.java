package top.gongtao.thread;

import java.util.concurrent.Callable;

/**
 * Created by gongtao on 2017/8/27 18:46.
 * 一个有返回值的线程
 */

public class MyThread3 implements Callable {
    @Override
    public Object call() throws Exception {
        System.out.print("一个有返回值的线程run the \"call()\" method!");
        return "test";
    }
}
