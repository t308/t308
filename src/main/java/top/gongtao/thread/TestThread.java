package top.gongtao.thread;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by gongtao on 2017/8/27 18:38.
 *
 *  Runnable , Callable 接口和多线程的实现没有关系
 *  接口的作用是约束行为， Runnable 接口的作用是指定一个协议，规定所有继承这些接口的类都要有一个无参无返回值的方法
 *
 *  多线程的实现是由类来完成的
 *  java 中所有的多线程都是通过 Thread 类来实现，线程的资源分配，线程启动这些工作都是在 start() 方法中完成的，严格
 *  来讲， 在 start0() 中完成的。start0() 是一个 native 方法， 并不是用 java 语言实现的。
 *
 *
 *  可以通过两种方式来定义多线程中要完成的工作
 *      1。实现 Runnable 接口
 *          Runnable 接口中的 run() 方法，没有返回值
 *      2。实现 Callable 接口
 *          Callable 接口中的 call() 方法，有返回值
 *  需要对线程进行管理(提交，启动，终止)
 *      1。Thread 类
 *          * Thread 类有一个构造函数可以传递一个 Runnable 类型的参数 target ，可以通过 target 来提供想要执行的任务
 *            （实现run()方法，然后传给 Thread 类）
 *          * Thread 类本身实现了 Runnable 接口，也可以直接通过实现 Thread 类来提交想要执行的任务(重写 run()方法)
 *          * Thread 类的 start() 方法负责启动执行线程，当 start() 方法执行时
 *              *若已经重写了run()方法来执行任务，则会执行该方法
 *              *若传入了 target 参数，则会调用 target 中的 run() 方法
 *          * Thread 类中有 stop 方法负责停止线程，但是已经弃用
 *          * 可以通过 interrupt() 方法终端线程
 *
 *
 */

public class TestThread {

    public static void main(String[] args){

        // 一个实现了Runnable接口的类
        MyThread thread = new MyThread();
        Thread t = new Thread(thread);
        t.start();//启动线程

        // 一个继承了Thread类的类
        MyThread2 thread2 = new MyThread2();
        Thread t2 = new Thread(thread2);
        t2.start();//启动线程


        // 创建一个ExecutorService
        ExecutorService executorService = Executors.newCachedThreadPool();
        // new一个MyThread线程,并交给executorService执行,
        // 通过Future接收返回结果
        Future future = executorService.submit(new MyThread3());
        try{
            // 从 future 中获取返回值
            Object result = future.get();
            System.out.println(result.toString());

        }catch(Exception ex){
            ex.printStackTrace();
        }
    }


}
