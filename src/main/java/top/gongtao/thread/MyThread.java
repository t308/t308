package top.gongtao.thread;

/**
 * Created by gongtao on 2017/8/27 18:37.
 * 实现 Runnable 接口
 */

public class MyThread implements Runnable {
    @Override
    public void run() {// 启动线程后执行的方法
        System.out.print("实现 Runnable 接口run the \"run()\" method!");
    }
}
