package top.gongtao.thread;

/**
 * Created by gongtao on 2017/8/27 18:41.
 * 继承 Thread 类
 */

public class MyThread2 extends Thread {

    @Override
    public void run() {// 启动线程后执行的方法
        System.out.print("继承 Thread 类 run the \"run()\" method!");
    }
}
