package top.gongtao.security.entities;

/**
 *  create by gongtao  2017/8/3 13:30.
 */

public class SysRole {
    private Integer id;
    private String name;
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}
