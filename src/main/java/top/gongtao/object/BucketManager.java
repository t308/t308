package top.gongtao.object;

import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.*;
import groovy.transform.ASTTest;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by gongtao on 2017/7/2.
 */
public class BucketManager {

    //显示所有 Bucket
    public static List<Bucket> getBucketList(){
        String endpoint = "https://oss-cn-shanghai.aliyuncs.com";
        String accessKeyId = "LTAIVVpoUiq8QnWM";
        String accessKeySecret = "mAMT5dzuMQ4iW8sVIr72trZV12LjVR";
        // 创建OSSClient实例
        OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
        try{
            // 列举bucket
            List<Bucket> buckets = ossClient.listBuckets();
            for (Bucket bucket : buckets) {
                System.out.println(" - " + bucket.getName());
            }
            return buckets;
        }catch (Exception ex){
            System.err.println(ex.toString());
            throw ex;
        }finally {
            // 关闭client
            ossClient.shutdown();
        }
    }

    //新增 Bucket
    public static boolean createBucket(String bucketName,String type){
        String endpoint = "https://oss-cn-shanghai.aliyuncs.com";
        String accessKeyId = "LTAIVVpoUiq8QnWM";
        String accessKeySecret = "mAMT5dzuMQ4iW8sVIr72trZV12LjVR";
        String typeStr = "";
        switch (type){
            case "private":
                typeStr="私有读";
                break;
            case "public-read":
                typeStr="公共读";
                break;
            case "public-read-write":
                typeStr="公共读写";
                break;
            case "default":
                typeStr="默认读";
                break;
        }
        // 创建OSSClient实例
        OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
        try{
            // 创建bucket
            // String bucketName = "gongtao-top";
            CreateBucketRequest createBucketRequest= new CreateBucketRequest(bucketName);
            // 将传入的 String 类型数据解析成枚举类型
            createBucketRequest.setCannedACL(CannedAccessControlList.parse(type));
            // 设置bucket存储类型为低频访问类型，默认是标准类型
            //createBucketRequest.setStorageClass(StorageClass.IA);
            ossClient.createBucket(createBucketRequest);

            System.out.println("新增bucket名为 " + bucketName + " 权限为 " + typeStr + " 类型");
            return true;
        }catch(Exception ex){
            System.err.println(ex.toString());
            return false;
        }finally {
            // 关闭client
            ossClient.shutdown();
        }
    }

    //移除 Bucket
    //如果存储空间不为空（存储空间中有文件或者分片上传碎片），则存储空间无法删除；
    //必须先删除存储空间中的所有文件后，存储空间才能成功删除。
    public static boolean removeBucket(String bucketName){
        // endpoint以杭州为例，其它region请按实际情况填写
        String endpoint = "https://oss-cn-shanghai.aliyuncs.com";
        // accessKey请登录https://ak-console.aliyun.com/#/查看
        String accessKeyId = "LTAIVVpoUiq8QnWM";
        String accessKeySecret = "mAMT5dzuMQ4iW8sVIr72trZV12LjVR";
        // 创建OSSClient实例
        OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
        // 删除bucket
        try{
            ossClient.deleteBucket(bucketName);
            System.out.println("bucket名为 " + bucketName + " 已被删除！！！");
            return true;
        }catch (Exception ex){
            System.err.println(ex.toString());
            return false;
        }finally {
            // 关闭client
            ossClient.shutdown();
        }
    }

    //查询指定 Bucket 是否存在
    public static boolean IsExistBucket(String bucketName){
        // endpoint以杭州为例，其它region请按实际情况填写
        String endpoint = "https://oss-cn-shanghai.aliyuncs.com";
        String accessKeyId = "LTAIVVpoUiq8QnWM";
        String accessKeySecret = "mAMT5dzuMQ4iW8sVIr72trZV12LjVR";
        OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
        boolean exists = ossClient.doesBucketExist(bucketName);
        System.out.print(bucketName+"是否存在："+exists);
        ossClient.shutdown();
        return exists;
    }

    //设置指定 Bucket 权限
    public static boolean changeBucketACI(String bucketName,String type){
        // endpoint以杭州为例，其它region请按实际情况填写
        String endpoint = "https://oss-cn-shanghai.aliyuncs.com";
        String accessKeyId = "LTAIVVpoUiq8QnWM";
        String accessKeySecret = "mAMT5dzuMQ4iW8sVIr72trZV12LjVR";
        OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
        String typeStr = "";
        switch (type){
            case "private":
                typeStr="私有读";
                break;
            case "public-read":
                typeStr="公共读";
                break;
            case "public-read-write":
                typeStr="公共读写";
                break;
            case "default":
                typeStr="默认读";
                break;
        }
        // 设置bucket权限 ,设置为私有读
        try{
            ossClient.setBucketAcl(bucketName, CannedAccessControlList.parse(type));
            System.out.println("bucket 名为 "+ bucketName +" 的权限已设置为 " + typeStr );
            return true;
        }catch(Exception ex){
            System.err.print(ex);
            return false;
        }finally {
            ossClient.shutdown();
        }

    }

    //获取指定 Bucket 权限
    public static String getBucketACI(String bucketName){
        // endpoint以杭州为例，其它region请按实际情况填写
        String endpoint = "https://oss-cn-shanghai.aliyuncs.com";
        String accessKeyId = "LTAIVVpoUiq8QnWM";
        String accessKeySecret = "mAMT5dzuMQ4iW8sVIr72trZV12LjVR";
        OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
        AccessControlList acl = ossClient.getBucketAcl(bucketName);
        // bucket权限
        System.out.println(acl.toString());
        ossClient.shutdown();
        return acl.toString();
    }

    //获取指定 Bucket 地区
    public static String getBucketLocation(String bucketName){
        // endpoint以杭州为例，其它region请按实际情况填写
        String endpoint = "https://oss-cn-shanghai.aliyuncs.com";
        String accessKeyId = "LTAIVVpoUiq8QnWM";
        String accessKeySecret = "mAMT5dzuMQ4iW8sVIr72trZV12LjVR";
        OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
        String location = ossClient.getBucketLocation(bucketName);
        System.out.println(location);
        ossClient.shutdown();
        return location;
    }

    //获取指定 Bucket info
    public static BucketInfo getBucketInfo(String bucketName){
        String endpoint = "https://oss-cn-shanghai.aliyuncs.com";
        String accessKeyId = "LTAIVVpoUiq8QnWM";
        String accessKeySecret = "mAMT5dzuMQ4iW8sVIr72trZV12LjVR";
        // 创建OSSClient实例
        OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
        BucketInfo info = ossClient.getBucketInfo(bucketName);
        // Location
        String location = info.getBucket().getLocation();
        // 创建日期
        Date createDate = info.getBucket().getCreationDate();
        // owner
        Owner owner = info.getBucket().getOwner();
        // 权限
        info.getGrants();
        System.out.print(bucketName+"的区域是"+location+"创建日期是："+createDate.toString()+"所有者是："+owner.toString());
        // 关闭client
        ossClient.shutdown();
        return info;
    }

}
