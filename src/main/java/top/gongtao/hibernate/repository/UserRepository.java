package top.gongtao.hibernate.repository;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import top.gongtao.entities.SysUser;

import javax.persistence.Table;

/**
 * Created by gongtao on 2017/8/5／11:29.
 *
 * 创建 UserRepository 接口，不需要实现类，SpringBoot 默认提供实现
 */


@Repository
@Table(name = "s_user")
@Qualifier("userRepository")
public interface UserRepository extends CrudRepository<SysUser,Integer>{
    public SysUser findOne(Integer id);

    public SysUser save(SysUser u);

    @Query("select t from SysUser t where t.name=:name")
    public SysUser findUserByName(@Param("name") String name);
}
