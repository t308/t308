package top.gongtao.hibernate.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import top.gongtao.entities.OAuthUser;

/**
 * Created by gongtao on 2017/8/26 20:08.
 */

public interface OauthUserRepository extends JpaRepository<OAuthUser, Integer> {

    OAuthUser findByOAuthTypeAndOAuthId(String oAuthType, String oAuthId);

}