package top.gongtao.listener;

/**
 *  create by gongtao  2017/8/11 09:13.  
 */

import javax.servlet.ServletContext;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

@WebListener
public class SessionListener implements HttpSessionListener {
    //Session创建事件
    @Override
    public void sessionCreated(HttpSessionEvent se) {
        ServletContext ctx = se.getSession( ).getServletContext( );
        Integer numSessions = (Integer) ctx.getAttribute("numSessions");
        if (numSessions == null) {
            numSessions = new Integer(1);
        }
        else {
            int count = numSessions.intValue( );
            numSessions = new Integer(count + 1);
        }
        System.out.println("当前人数为:"+numSessions);
        ctx.setAttribute("numSessions", numSessions);
    }
    //Session销毁事件
    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        ServletContext ctx=se.getSession().getServletContext();
        Integer numSessions = (Integer)ctx.getAttribute("numSessions");
        if(numSessions == null){
            numSessions = new Integer(0);
        } else {
            int count = numSessions.intValue( );
            numSessions = new Integer(count - 1);
        }
        ctx.setAttribute("numSessions", numSessions);
        System.out.println("当前人数为:"+numSessions);
    }
}
