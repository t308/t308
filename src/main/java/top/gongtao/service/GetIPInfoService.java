package top.gongtao.service;

import top.gongtao.utils.HttpUtils;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by gongtao on 2017/6/21 20:02.
 */
public class GetIPInfoService {

    public String getIP() throws UnknownHostException {
        String ipStr = "";
        String host = "https://dm-81.data.aliyun.com";
        String path = "/rest/160601/ip/getIpInfo.json";
        String method = "GET";
        String appcode = "912c70107b59452fa705ef95b6124c21";
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "APPCODE " + appcode);
        Map<String, String> querys = new HashMap<String, String>();
        querys.put("ip", InetAddress.getLocalHost().getHostAddress());


        try {
            HttpResponse response = HttpUtils.doGet(host, path, method, headers, querys);
            ipStr = EntityUtils.toString(response.getEntity());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ipStr;
    }
}
