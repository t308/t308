package top.gongtao.service;

import top.gongtao.utils.HttpUtils;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by gongtao on 2017/6/21 22:44.
 */
public class GetNewsService {

    public String getNews(String type){
        String news="";
        String host = "http://toutiao-ali.juheapi.com";
        String path = "/toutiao/index";
        String method = "GET";
        String appcode = "912c70107b59452fa705ef95b6124c21";
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "APPCODE " + appcode);
        Map<String, String> querys = new HashMap<String, String>();
        querys.put("type", type);


        try {
            HttpResponse response = HttpUtils.doGet(host, path, method, headers, querys);
            news = EntityUtils.toString(response.getEntity());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return news;
    }
}
