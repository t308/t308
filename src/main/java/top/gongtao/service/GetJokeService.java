package top.gongtao.service;

import top.gongtao.utils.HttpUtils;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by gongtao on 2017/6/22 23:22.
 */
public class GetJokeService {
    public String getJoke(String pagesize,String currentpage){
        String joke="";
        String host = "https://ali-joke.showapi.com";
        String path = "/textJoke";
        String method = "GET";
        String appcode = "912c70107b59452fa705ef95b6124c21";
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "APPCODE " + appcode);
        Map<String, String> querys = new HashMap<String, String>();
        querys.put("maxResult", pagesize);
        querys.put("page", currentpage);
        querys.put("time", "2016-07-01");


        try {
            HttpResponse response = HttpUtils.doGet(host, path, method, headers, querys);
            joke = EntityUtils.toString(response.getEntity());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return joke;
    }
}
