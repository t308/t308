package top.gongtao.service;

import top.gongtao.utils.HttpUtils;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by gongtao on 2017/6/20 19:33.
 */
public class WetherServer {
    public String getWether(String area){
        String host = "http://saweather.market.alicloudapi.com";
        String path = "/area-to-id";
        String method = "GET";
        String appcode = "912c70107b59452fa705ef95b6124c21";
        Map<String, String> headers = new HashMap<String, String>();
        //最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
        headers.put("Authorization", "APPCODE " + appcode);
        Map<String, String> querys = new HashMap<String, String>();
        querys.put("area", area);
        String wetherJson = "";

        try {
            HttpResponse response = HttpUtils.doGet(host, path, method, headers, querys);
            wetherJson = EntityUtils.toString(response.getEntity());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return wetherJson;

    }
}

