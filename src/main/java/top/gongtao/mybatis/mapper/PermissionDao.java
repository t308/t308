package top.gongtao.mybatis.mapper;

/**
 *  create by gongtao on 2017/8/7 12:39.
 */

import org.apache.ibatis.annotations.Mapper;
import top.gongtao.security.entities.Permission;

import java.util.List;

@Mapper
public interface PermissionDao {
    public List<Permission> findAll();
    public List<Permission> findByAdminUserId(int userId);
}
