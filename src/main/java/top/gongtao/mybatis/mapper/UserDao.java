package top.gongtao.mybatis.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import top.gongtao.security.entities.SysUser;

/**
 * create by gongtao on 2017/8/3 13:31.
 */
@Mapper
public interface UserDao {

    public SysUser findByUserName(String username);

}
