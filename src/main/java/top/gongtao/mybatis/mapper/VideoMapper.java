package top.gongtao.mybatis.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import top.gongtao.entities.User;
import top.gongtao.entities.Video;

import java.util.List;

/**
 * Created by gongtao on 2017/7/17.
 */

@Mapper
public interface VideoMapper {

    //获取所有视频信息
    @Select("select * from lx_video")
    public List<Video> getAll();


    //通过视频ID获取URL
    @Select("select url from lx_video where id=#{id}")
    public String getUrlById(@Param("id") String id);

}
