package top.gongtao.mybatis.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import top.gongtao.entities.IndexTitle;

import java.util.List;

/**
 * Created by gongtao on 2017/8/2.
 */

@Mapper
public interface IndexTitleMapper {
    @Select("select * from lx_indextitle where parentnodeid=0")
    public List<IndexTitle> getAll();

}
