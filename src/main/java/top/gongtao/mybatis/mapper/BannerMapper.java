package top.gongtao.mybatis.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import top.gongtao.entities.Images;

import java.util.List;

/**
 * Created by gongtao on 2017/8/1.
 */

@Mapper
public interface BannerMapper {
    //获取所有会员信息
    @Select("select gi.ID,gi.URL,gi.LINKADDRESS,gi.TITLE,gi.TYPEID,gi.SERIESNUMBER,gi.PREVIEWURL,gi.publishtime,gi.`describe`,gi.TEMPLATEID from gt_images gi INNER join gt_images_type git on gi.TYPEID = git.ID where gi.TYPEID=1;")
    public List<Images> find();


}
