package top.gongtao.mybatis.mapper;

import top.gongtao.entities.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created by gongtao on 2017/7/2.
 */


@Mapper
public interface UserMapper {

    //获取所有会员信息
    @Select("select * from lx_user")
    public List<User> find();

    //添加会员信息
    @Insert("insert into lx_user(mail,nickname,sex,age,headimage,username,`password`,createtime,fanscount,attentioncount)\n" +
            " \tvalues(#{mail},#{username},1,20,'',#{username},#{password},SYSDATE(),0,0)")
    public void addUser(@Param("mail") String mail,@Param("username") String username,@Param("password") String password);

    //获取指定邮箱是否已被注册
    @Select("select count(1) from lx_user where mail = #{mail}")
    public int getIsRegisteredOfMail(@Param("mail") String mail);

    //获取指定用户名是否已被注册
    @Select("select count(1) from lx_user where username = #{username}")
    public int getIsRegisteredOfUsername(@Param("username") String username);

    //获取用户输入用户名和密码在数据库中是否存在
    @Select("select count(1) from lx_user where username = #{username} and 'password' = #{password}")
    public int getIsExitOfAccount(@Param("username") String username,@Param("password") String password);

    //获取指定用户名的密码
    @Select("select `password` from lx_user where username = #{username}")
    public String getPasswordByUsername(@Param("username") String username);
}


