package top.gongtao.github;

import org.scribe.model.*;
import org.scribe.oauth.OAuthService;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONPath;
import top.gongtao.entities.OAuthUser;
import top.gongtao.entities.SysUser;
import top.gongtao.entities.User;
import top.gongtao.oauth.OAuthTypes;

/**
 * Created by gongtao on 2017/8/26 20:11.
 */

public class GithubOAuthService extends OAuthServiceDeractor {

    private static final String PROTECTED_RESOURCE_URL = "https://api.github.com/user";

    public GithubOAuthService(OAuthService oAuthService) {
        super(oAuthService, OAuthTypes.GITHUB);
    }

    @Override
    public OAuthUser getOAuthUser(Token accessToken) {
        OAuthRequest request = new OAuthRequest(Verb.GET, PROTECTED_RESOURCE_URL);
        this.signRequest(accessToken, request);
        Response response = request.send();
        OAuthUser oAuthUser = new OAuthUser();
        oAuthUser.setoAuthType(getoAuthType());
        Object result = JSON.parse(response.getBody());
        oAuthUser.setoAuthId(JSONPath.eval(result, "$.id").toString());
        oAuthUser.setUser(new SysUser());
        oAuthUser.getUser().setName(JSONPath.eval(result, "$.login").toString());
        return oAuthUser;
    }


    @Override
    public Token getRequestToken() {
        return null;
    }

    @Override
    public Token getAccessToken(Token requestToken, Verifier verifier) {
        return null;
    }

    @Override
    public void signRequest(Token accessToken, OAuthRequest request) {

    }

    @Override
    public String getVersion() {
        return null;
    }

    @Override
    public String getAuthorizationUrl(Token requestToken) {
        return null;
    }
}