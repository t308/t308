package top.gongtao.github;

import org.scribe.builder.api.DefaultApi20;
import org.scribe.model.OAuthConfig;
import org.scribe.utils.OAuthEncoder;

/**
 * Created by gongtao on 2017/8/25 23:15.
 */

public class GithubApi extends DefaultApi20 {

    public static final String AUTHORIZE_URL = "https://github.com/login/oauth/authorize?client_id=%s&redirect_uri=%s&state=%s";
    private static final String SCOPED_AUTHORIZE_URL = AUTHORIZE_URL + "&scope=%s";
    private static final String ACCESS_TOKEN_URL = "https://github.com/login/oauth/access_token?state=%s";

    private final String githubState;

    public GithubApi(String githubState) {
        this.githubState = githubState;
    }


    @Override
    public String getAccessTokenEndpoint() {
        return String.format(ACCESS_TOKEN_URL,githubState);
}

    @Override
    public String getAuthorizationUrl(OAuthConfig config) {
        if (config.hasScope()){
            return String.format(SCOPED_AUTHORIZE_URL, config.getApiKey(), OAuthEncoder.encode(config.getCallback()),
                    githubState, OAuthEncoder.encode(config.getScope()));
        }
        else{
            return String.format(AUTHORIZE_URL, config.getApiKey(), OAuthEncoder.encode(config.getCallback()), githubState);
        }
    }
}
