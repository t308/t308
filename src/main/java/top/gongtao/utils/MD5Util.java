package top.gongtao.utils;

//BASE64Decoder这个没有Java doc，属于jdk里不推荐使用的工具类 强烈建议不要用sun.misc，是不安全的 ，最好不要使用 使用apache common中的Base64替换
//import sun.misc.BASE64Encoder;
import org.apache.commons.codec.binary.Base64;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by gongtao on 2017/7/2.
 * 在各种应用系统的开发中，经常需要存储用户信息，很多地方都要存储用户密码，而将用户密码直接存储在服务器上显然是不安全的
 */
public class MD5Util {

    /**利用MD5进行加密
     * @param str  待加密的字符串
     * @return  加密后的字符串
     * @throws NoSuchAlgorithmException  没有这种产生消息摘要的算法
     * @throws UnsupportedEncodingException
     */
    public static String encoderByMd5(String str) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        //确定计算方法
        MessageDigest md5= MessageDigest.getInstance("MD5");
        Base64 base64en = new Base64();
        //加密后的字符串
        String newstr=base64en.encodeAsString(md5.digest(str.getBytes("utf-8")));
        return newstr;
    }

    /**判断用户密码是否正确
     * @param newpasswd  用户输入的密码
     * @param oldpasswd  数据库中存储的密码－－用户密码的摘要
     * @return
     * @throws NoSuchAlgorithmException
     * @throws UnsupportedEncodingException
     */
    public static boolean checkPassword(String newpasswd,String oldpasswd) throws NoSuchAlgorithmException, UnsupportedEncodingException{
        if(encoderByMd5(newpasswd).equals(oldpasswd))
            return true;
        else
            return false;
    }
}
