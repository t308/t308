package top.gongtao.config;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by gongtao on 2017/8/5／22:36.
 * Spring自带支持定时器的任务实现。实现定时器任务的功能，规定多少秒之后要做什么，多少秒应该怎样之类
 */

@Component
@Configurable
@EnableScheduling   //标注启动定时任务
public class ScheduledTasks {
    @Scheduled(fixedRate = 1000 * 30)   //定义某个定时任务
    public void reportCurrentTime(){
        System.out.println ("Scheduling Tasks Examples: The time is now " + dateFormat ().format (new Date()));
    }

    //每1分钟执行一次
    @Scheduled(cron = "0 */1 *  * * * ")
    public void reportCurrentByCron(){
        System.out.println ("Scheduling Tasks Examples By Cron: The time is now " + dateFormat ().format (new Date ()));
    }

    private SimpleDateFormat dateFormat(){
        return new SimpleDateFormat ("HH:mm:ss");
    }
}
