package top.gongtao.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;
import top.gongtao.interceptor.MyFilterSecurityInterceptor;
import top.gongtao.service.CustomUserService;

import javax.sql.DataSource;

/**
 * Created by gongtao on 2017/5/14 13:58.
 *
 * Spring Security 是专门针对jiyu
 *
 *
 *
 */


@Configuration
//Spring Security 与 Spring MVC 的配置类似，只需要在一个配置类上注解 @EnableWebSecurity 并且让这个类继承 WebSecurityConfigurerAdapter 即可
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{

    @Autowired
    private MyFilterSecurityInterceptor myFilterSecurityInterceptor;


    @Bean
    UserDetailsService customUserService(){ //注册UserDetailsService 的bean
        return new CustomUserService();
    }

    //通过重写 configure 方法来配置相关的安全配置。
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //允许所有用户访问"/"和"/home"
        http.authorizeRequests()
                .antMatchers("/","/index.html", "/home","/backend/login.html","login.html","/sign-up.html","/find_password.html","/swagger-ui.html","/login").permitAll()
                //其他地址的访问均需验证权限
                .anyRequest().authenticated()
                .and()
                .formLogin()
                //指定登录页是"/login"
                .loginPage("/login.html")
                .loginProcessingUrl("/login")//登陆处理路径 
                .usernameParameter("user_name")//登陆用户名参数  
                .passwordParameter("user_password")//登陆密码参数  
                .defaultSuccessUrl("/")//登录成功后默认跳转到"/"
                .failureUrl("/login-error.html")//登陆失败路径
                .permitAll()
                .and()
                .logout()
                .logoutSuccessUrl("/login.html")//退出登录后的默认url是"/login"
                .permitAll();
        http.addFilterBefore(myFilterSecurityInterceptor, FilterSecurityInterceptor.class);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        super.configure(auth);
//        auth.inMemoryAuthentication()
//                .withUser("gongtao").password("gt199564").roles("GT_ADMIN")
//                .and()
//                .withUser("gt").password("gt").roles("GT_USER");

        auth.userDetailsService(customUserService()); //user Details Service验证


//        auth.jdbcAuthentication().dataSource(dataSource);

//        auth.jdbcAuthentication().dataSource(dataSource)
//                .usersByUsernameQuery("select username,password,true from lx_user where username = ?")
//                .authoritiesByUsernameQuery("select username,role from roles where username=?");

    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        super.configure(web);
    }
}
