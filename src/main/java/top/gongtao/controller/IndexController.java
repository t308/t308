package top.gongtao.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import sun.misc.Request;
import top.gongtao.hibernate.repository.UserRepository;
import top.gongtao.mybatis.mapper.BannerMapper;
import top.gongtao.mybatis.mapper.IndexTitleMapper;
import top.gongtao.mybatis.mapper.VideoMapper;
import top.gongtao.utils.MD5Util;
import top.gongtao.utils.MailUtil;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

/**
 * Created by gongtao on 2017/7/17.
 */

@Controller
@RequestMapping("/")
@Transactional
public class IndexController {

    Logger logger = LoggerFactory.getLogger(IndexController.class);

    @Autowired
    private VideoMapper videoMapper;

    @Autowired
    private BannerMapper bannerMapper;

    @Autowired
    private IndexTitleMapper indexTitleMapper;

    @Autowired
    private UserRepository userRepository;

    private boolean isLogin;

    //接受登陆请求
    @RequestMapping(value="login.html",method=RequestMethod.GET)
    public String preLogin(HttpServletRequest request){
        isLogin = (SecurityContextImpl) request.getSession().getAttribute("SPRING_SECURITY_CONTEXT")==null?false:true;
        if(isLogin){
            return "redirect:/index.html";
        }
        return "web/login";
    }

    //接受注册请求
    @RequestMapping(value = "sign-up.html", method=RequestMethod.GET)
    public String preSignUp(){
        return "web/sign-up";
    }

    // Login form with error
    @RequestMapping("/login-error.html")
    public String loginError(Model model,HttpServletRequest request) {
        isLogin = (SecurityContextImpl) request.getSession().getAttribute("SPRING_SECURITY_CONTEXT")==null?false:true;
        if(isLogin){
            return "redirect:/index.html";
        }
        model.addAttribute("loginError", true);
        return "web/login";
    }

    //首页控制器
    @RequestMapping(value = "/index.html",method = RequestMethod.GET)
    public String index(Model model,HttpServletRequest request){
        logger.info("这是日志信息");
        logger.error("这是报错信息");
        model.addAttribute("videos",videoMapper.getAll());
        //获取首页轮播图
        model.addAttribute("bannerImages",bannerMapper.find());
        //获取首页标题
        model.addAttribute("indexTitle",indexTitleMapper.getAll());
        model.addAttribute("indexTitle",indexTitleMapper.getAll());
        return "web/index";
    }

    //首页控制器
    @RequestMapping(value = "/",method = RequestMethod.GET)
    public String index2(Model model,HttpServletRequest request){
        model.addAttribute("videos",videoMapper.getAll());
        //获取首页轮播图
        model.addAttribute("bannerImages",bannerMapper.find());
        //获取首页标题
        model.addAttribute("indexTitle",indexTitleMapper.getAll());
        return "web/index";
    }

    @RequestMapping(value = "play/{id}",method=RequestMethod.GET)
    public String playVideo(@PathVariable("id") String id,Model model){
        model.addAttribute("url",videoMapper.getUrlById(id));
        return "web/vplay";

    }

    //提交注册表单信息
    @ResponseBody
    @PostMapping("sign-up.html")
    public String signUp(@RequestParam String user_email,@RequestParam String user_name,@RequestParam String user_password,HttpServletRequest request){
        return "hello "+user_name;
    }


}
