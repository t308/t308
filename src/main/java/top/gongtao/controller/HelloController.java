package top.gongtao.controller;
import io.swagger.annotations.ApiOperation;
import top.gongtao.mybatis.mapper.UserMapper;
import top.gongtao.service.GetJokeService;
import top.gongtao.service.GetNewsService;
import top.gongtao.service.Wether24Server;
import top.gongtao.utils.MD5Util;
import top.gongtao.utils.MailUtil;
import top.gongtao.service.*;
import top.gongtao.service.GetIPInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ui.Model;
import org.springframework.stereotype.*;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

/**
 * Created by GongTao on 2017/3/26 20:51.
 */


/**
 * 在常规的 Spring 环境下，注入 properties 文件里的值得方式，通过 @PropertySource 指明 properties 文件的位置，然后通过 @Value
 * 注入值。在 Spring Boot 中，我们只需要在 application.properties 中定义属性，直接使用 @Value 注入即可
 */

@Controller
@RequestMapping("/backend")
public class HelloController {

    @Autowired
    UserMapper userMapper;

    @Value("${gongtao.name}")
    private String name;

    //接受登陆请求
    @RequestMapping(value="login.html",method=RequestMethod.GET)
    public String preLogin(){
        return "login";
    }

    //提交登陆表单信息
    @ResponseBody
    @RequestMapping(value="login.html",method=RequestMethod.POST)
    public String login(@RequestParam String username,@RequestParam String password,@RequestParam String type, HttpServletRequest request) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        boolean result = MD5Util.checkPassword(password,userMapper.getPasswordByUsername(username));    //获取传入参数用户数据库中MD5加密后的密码和传入的密码使用 checkPassword() 方法返回是否登陆成功
        if(result) {
            if(type.equals("1")){
                request.getSession().removeAttribute("cur_user");
                request.getSession().setAttribute("cur_user",username);
            }else if(request.getSession().getAttribute("cur_user") == null ){      //当前没有用户登陆
                request.getSession().setAttribute("cur_user",username);
            }else{
                return "{\"success\":-1,\"cur_username\":\""+ request.getSession().getAttribute("cur_user").toString() +"\"}";          //提示用户是否注销已登陆用户并使用当前账号登陆
            }
        }
        return "{\"success\":"+ result +"}";
    }



    //接受注册请求
    @RequestMapping(value = "sign-up.html", method=RequestMethod.GET)
    public String preSignUp(){
        return "sign-up";
    }

    //提交注册表单信息
    @ResponseBody
    @RequestMapping(value = "sign-up.html",method = RequestMethod.POST)
    public String signUp(@RequestParam String email,@RequestParam String username,@RequestParam String password,HttpServletRequest request){
        String message = "";
        try{
            if(userMapper.getIsRegisteredOfMail(email)!=0){                 //检验邮箱是否注册过
                message = "{\"success\":-2}";       //邮箱已被注册  -2
            }else if(userMapper.getIsRegisteredOfUsername(username)!=0){    //检验用户名是否唯一
                message = "{\"success\":-3}";       //用户名已被注册  -3
            }else{
                userMapper.addUser(email,username, MD5Util.encoderByMd5(password));  //保存用户信息 密码使用 MD5 加密
                MailUtil.sendMail("postmaster@gongtao.top",email,"postmaster@gongtao.top","Gt199564@","【T308】gongtao,您好，欢迎加入T308","您的验证码是33450");
                message =  "{\"success\":0}";       //注册成功  0
                request.getSession().setAttribute("cur_user",username);
            }
        }catch(Exception ex){
            System.err.println("注册出错："+ex.toString());
            message = "{\"success\":-1}";           //代码错误  -1
        }
        return message;
    }



    @RequestMapping(value="exit.html",method=RequestMethod.GET)
    public void logOut(HttpServletRequest request) throws SQLException {
        request.removeAttribute("cur_user");
    }

    @RequestMapping(value="/index",method=RequestMethod.GET)
    public String index(Model model,HttpServletRequest request) throws Exception {
        model.addAttribute("cur_user_username",request.getSession().getAttribute("cur_user"));      //将当前用户名添加到model中
        return "index";
    }

    @ResponseBody
    @RequestMapping(value="getWether",method=RequestMethod.GET,produces={"text/html;charset=UTF-8;","application/json;"})
    public String getWetherMessage(){
        Wether24Server wetherServer = new Wether24Server();
        return wetherServer.getWether("上海");
    }

    //获取当前用户 IP
    @ResponseBody
    @RequestMapping(value="getIp",method=RequestMethod.GET,produces={"text/html;charset=UTF-8;","application/json;"})
    public String getIp(HttpServletRequest request) throws UnknownHostException {
        //return HttpUtils.getIpAddr(request);
        return new GetIPInfoService().getIP();
    }
    //获取新闻
    /*
    类型,,top(头条，默认),shehui(社会),guonei(国内),guoji(国际),yule(娱乐),tiyu(体育)junshi(军事),keji(科技),caijing(财经),shishang(时尚)
     */
    @ApiOperation(value="新闻获取接口", notes="可以通过传入type类型获取不同类型的新闻")
    @ResponseBody
    @RequestMapping(value="getNews", method=RequestMethod.GET,produces={"text/html;charset=UTF-8;","application/json;"})
    public String getNews(@RequestParam(required = false) String type){
        return new GetNewsService().getNews(type);
    }

    @ResponseBody
    @RequestMapping(value="getJoke", method=RequestMethod.GET,produces={"text/html;charset=UTF-8;","application/json;"})
    public String getNews(@RequestParam String pagesize,@RequestParam String currentpage){
        return new GetJokeService().getJoke(pagesize,currentpage);
    }

    @RequestMapping(value="tableList",method=RequestMethod.GET)
    public String testTableList(){
        return "table-list";
    }

    @RequestMapping(value="tablejoke",method=RequestMethod.GET)
    public String testTableJoke(){
        return "table-joke";
    }

    @RequestMapping(value = "testWeb",method = RequestMethod.GET)
    public String testWeb(){
        return "web/index";
    }



}