package top.gongtao.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import top.gongtao.entities.SysUser;
import top.gongtao.hibernate.repository.UserRepository;

import java.util.Date;

/**
 * Created by gongtao on 2017/8/5／11:34.
 */

@Controller
@RequestMapping("/Hibernate")
@EnableAutoConfiguration
public class HibernateController {

    //注入 SpringBoot 实现的接口
    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "getUserById",method= RequestMethod.GET)
    @ResponseBody
    public String getUserById(Integer id){
        String user = userRepository.findAll().toString();
//        System.out.println("userRepository: " + userRepository);
//        System.out.println("id: " + id);
        return user;
    }

    @RequestMapping("saveUser")
    @ResponseBody
    public void saveUser() {
        SysUser u = new SysUser();
        u.setName("gongtao");
        u.setEmail("330556994@qq.com");
        u.setPassword("Gt199564@");
        u.setDob(new Date());
        userRepository.save(u);
    }

}
